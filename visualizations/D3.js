D3.js  Data-Driven documents

From the webpage: (mbostock.github.com/d3/)

    D3 allows you to bind arbitrary data to a Document Object Model
    (DOM), and then apply data-driven transformations to the
    document. As a trivial example, you can use D3 to generate a basic
    HTML table from an array of numbers. Or, use the same data to
    create an interactive SVG bar chart with smooth transitions and
    interaction.

    D3 is not a traditional visualization framework. Rather than
    provide a monolithic system with all the features anyone may ever
    need, D3 solves only the crux of the problem: efficient
    manipulation of documents based on data. This gives D3
    extraordinary flexibility, exposing the full capabilities of
    underlying technologies such as CSS3, HTML5 and SVG. It avoids
    learning a new intermediate proprietary representation. With
    minimal overhead, D3 is extremely fast, supporting large datasets
    and dynamic behaviors for interaction and animation. And, for
    those common needs, D3’s functional style allows code reuse
    through a diverse collection of optional modules."

Sort of similar to jQuery, in that you still need to create your
documents using traditional techniques and you still need to be
familiar with HTML5, SVG, the DOM, etc.

Superceeds Protovis
