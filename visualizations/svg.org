see https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial

* Basic example:
<svg version="1.1"
     baseProfile="full"
     width="300" height="200"
     xmlns="http://www.w3.org/2000/svg">

  <rect width="100%" height="100%" fill="red" />
  <circle cx="150" cy="100" r="80" fill="green" />
  <text x="150" y="125" font-size="60" text-anchor="middle" fill="white">SVG</text>
</svg>

Elements:
* <svg>
** attributes
-width
-height
-version (use 1.1)
-xmlns (http://www.w3.org/2000/svg)

* basic shapes:
<rect>
- attrs: x, y, width, height, rx, ry (last two are corner radii)

<circle>
- attrs: r, cx, cy

<ellipse>
- attrs: rx, ry, cx, cy (radii, center)

<line>
- attrs: x1, y1, x2, y2

<polyline>
- attrs: points (eg <polyline points='23 54, 55 29, 203 84' />
  
<polygon>
- attrs: points (same as polyline, but automatically closes)

* fill and stroke
These are both attributes for the other line/shape commands.
You can also use CSS to specify these attributes.  You can also
stick them in a <defs> element.

- stroke="color"
- stroke-opacity="1.0"
- stroke-width="2" (in pixels)
- stroke-linecap="butt" (or "square", "round", ?)
- stroke-linejoin="miter" (or "round", "bevel", ?)
- stroke-dasharray="5,5" (or "5,10,3,...")
- stroke-miterlimit

- fill="color"
- fill-opacity="0.5"
- file-rule

* <path>
** attributes
-d: defines path, using a series of command/args
- - Line commands:
- - M x y: move to (x,y)
- - m dx dy: move to, relative to current position
- - L x y: line to (x, y)
- - l dx dy: line to, relative to current position
- - H x: horizontal line 
- - V y: vertical line
- - z: draw straight line back to start (closes loop)
- - example: <path d="M10 10 H 90 V 90 H 10 L 10 10"/>

- - Curve commands:
- - C x1 y1, x2 y2, x y (first two points are control points, third point is endpoint of curve)
- - c dx1 dy1, dx2 dy2, dx dy (relative version)
    curves start out in direction of first point, end in direction of second
- - S does something
- - Q does something
- - T does something, too

- - Arc Commands (sections of circles or ellipses)
- - A rx ry x-axis-rotation large-arc-flag sweep-flag x y
    rx, ry: x and y radius
    x-axis-rotation, large-arc-flag: flags to determine direction and sweep of arc
    x, y: endpoint of arc (or dx, dy)

    
