Query interface: should be eff-in cool
"It's time for a beautiful network"
Natural language queries?  (personal opinion: ugh)
Need a truly new interface to queries; graphical?

------------------------------------------------------------------------

* Use cases
Config management does exist today, but primitive...
Yang-based modeling?

event streams to replay history in a new dataservice
this is a provenance thing
would things like react.js help?

Truman:
Look at whole picture;
drill down
health of ind. elements (possibly in popup-realtime)
exploratory

explore at a services layer; eg, look at all applications, databases, 
Brett: service api translates user questions to dataservice layer questions (et al)

Truman:
text-box search (for power users)

Brett:
cli search should use auto-complete

<how to encapsulate a dsl-type entity or description of a domain for the 
service layer to utilize and apply/translate to the data layer?  Would 
describe queries and "provide opionions" and the translations needed...>
<would be sorta like a skin>

<nother thought: create a profile that describes that "happy" state of the 
network; anytime your network goes out of that profile, alerts are sent, or 
some other notifications or automated snapshots are created>


------------------------------------------------------------------------
want to know/discover topologies

more queries:
want to ask hosts/machines questions; 
path between nodes
geographical data of hosts
uuid -> machine translation


Need a "toolbox" of statistics run on quantities exported by the data service,
and some way to allow the user to easily use those tools.

listener pattern for service layer; 

what is the api for the data service:
- list all top-level keys
- list all key combinations (eg snd_metadata.snd_uuid)
- list all edge types
- get all agents
- for any node, get its subtree
- for any node, get its parent (or any edge type)
- random gremlin traversals?
- find all nodes with key or key/value
- all values for key?

- subscribe to events: change in state of any key/value pair (within range?)
  (maybe within context of node type?)
- allow creation of complex filters of event subscriptions, with ability to respond
- log/replay/capture events
- visualization support 




