The Workload Identity Foundation allows cross platform authorization
with any provider that supports OpenID Connect (OIDC).  It's a Google
service/product/whatever.  It is useful for replacing access to
resources without using access keys, which can leak, need to be rotated,
and are generally a pain.

[[https://cloud.google.com/iam/docs/workload-identity-federation][Workload Identity Foundation Docs]] (GCP)

* Video tutorial w/AWS
[[https://www.youtube.com/watch?v=wgzuLEdHcO0][AWS to GCP sans service account keys!! - Workload Identity Federation]] (YouTube)

1. Create a service account
2. Create AWS EC2 instances
3. Goto GCP->IAM->WIF, create identity provider/pool
4. Grant access to SA

   
