* Slack docs
[[https://api.slack.com/apps/A07D53ZKYTZ/general?][Building Slack Apps]]

* Zonar
[[https://gitlab.com/ZonarSystems/data-services/ci-scripts-image/-/blob/main/README.md][rok8 script for releasing slack events (et al)]]
[[https://zonarsystems.atlassian.net/wiki/spaces/DDOS/pages/137725739015/Slack+Integration][Slack Integration at Zonar]] (Confluence)

