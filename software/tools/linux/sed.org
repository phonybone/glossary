* Sed
[[https://www.gnu.org/software/sed/manual/sed.html][Overview]]

commands are of general form [addr]X[opts] where
'addr' is a line selector; can be a range ('30,35'), a regex ('/foo/'), etc
'X' is a one-letter command
'opts' are options to X

You can use '-e' to give more than one command

** Commands
'a': append to line
'b': branch to label
'c': replace text
'q': quit
'd': delete line

** Examples
echo 'cloned onroute[86] -> 100' | sed 's/cloned onroute.*-> //'  # yields 100
