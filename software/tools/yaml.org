YAML notes

https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html

* Start and end doc: --- and ...
These two symbols can be used to start and end the yaml document

* Comments
^# is a comment
': ' is a comment (but it's also the key/value separator, so...?)

* Nested Lists/Dicts:
'-' is the list item marker.  All items starting w/'-', w/same indentation level, are part of 
a list.

EG:
# Employee records
-  martin:
    name: Martin D'vloper
    job: Developer
    skills:
      - python
      - perl
      - pascal
-  tabitha:
    name: Tabitha Bitumen
    job: Developer
    skills:
      - lisp
      - fortran
      - erlang

Equivalent JSON:
[{
  'martin': {
    'name': "Martin D'vloper"
    'job': "Developer"
    'skills': [
      'python',
      'perl',
      'pascal',
      ]
   },
  "tabitha": {
    "name": "Tabitha Bitumen"
    "job": "Developer"
    "skills": [
      "lisp",
      "fortran",
      "erlang"
      ]
  }
}]

* Inline Lists/Dicts:
martin: {name: Martin D'vloper, job: Developer, skill: Elite}
fruits: ['Apple', 'Orange', 'Strawberry', 'Mango']


* Multi-line values: | and >
Indentation ignored in both cases
** | Literal Block Scalar
Keeps newlines intact
** > Folded Block Scalar
Newlines become spaces



* Aliases
https://github.com/cyklo/Bukkit-OtherBlocks/wiki/Aliases-(advanced-YAML-usage)
# actually, that one kind of sucks
instead see https://gist.github.com/ddlsmurf/1590434
also https://medium.com/@kinghuang/docker-compose-anchors-aliases-extensions-a1e4105d70bd
also, use ~/.bash.functions:ytoj 
Basic idea: define/recall data (string, list, block, whatevs)

Use '&' to define the alias
Use '*' to expand the alias

** Example 1
key_one: &anchor_val 123
key_two: &anchor
  sub_key: *anchor_val
key_three: *anchor

becomes

{
    "key_one": 123,
    "key_two": {
        "sub_key": 123
    },
    "key_three": {
        "sub_key": 123
    }
}

anchor_val => 123
ancor => {sub_key: *anchor_val} => {sub_key: 123}

** Example 2: flow merge
Use the <<: to insert the contents of an alias into a dict/hash:

key_one: &anchor
  sub_key_one: value one
  sub_key_two: value two
key_two:
  sub_key_three: value three
  <<: *anchor

becomes

{
    "key_one": {
        "sub_key_one": "value one",
        "sub_key_two": "value two"
    },
    "key_two": {
        "sub_key_one": "value one",
        "sub_key_two": "value two",
        "sub_key_three": "value three"
    }
}
** Example 3: taken from zapy_zpassplus/.gitlab-ci.yml
.setup_ssh: &setup_ssh |
      eval $(ssh-agent -s)
      echo -e "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
      mkdir -m 700 -p ~/.ssh
      ssh-keyscan github.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts

.setup_working:
  - &setup-working
    export GCP_PROJECT=zonar-working;
    export GCLOUD_KEY=$GCLOUD_KEY_WORKING
    export CLUSTER_NAME=vpc-native-working-1-kube-zonar;
    prepare-gcloud
# then:
  before_script:
    - *setup-working
* Gotchas
[[https://noyaml.com/][Nobody Wants to Write YAML]]
