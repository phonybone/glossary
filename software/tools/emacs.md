
# Table of Contents

1.  [Links/bookmarks](#orgbd224b8)
2.  [Lisp](#org093c83a)
3.  [python & elpy](#orgffcf852)
4.  [VCS](#orgd2d33d5)
    1.  [magit](#orgbd10f0f)
    2.  [vc-diff](#orgaacd7b5)
    3.  [virtual envs](#org2b7cd91)
    4.  [elpy](#orgeefe621)
        1.  [linting](#org6f72618)
    5.  [code-folding](#org7260f41)
        1.  [pyenv](#org21a48f5)
5.  [CEDET (Collection of Emacs Development Environment Tools)](#orgc609313)
6.  [packages:](#orgde0cafb)
    1.  [M-x list-packages](#org0431c31)
    2.  [M-x package-refresh-contents](#orga6312b9)
    3.  [M-x package-install <ret> <package-name&#x2026;>](#org0446037)
    4.  [direx](#org1939ea6)
    5.  [org-asciidoc](#orgbd5b402)
7.  [yasnippets](#org2b4f6b1)
    1.  [Commands:](#org3588f3a)
8.  [editing remote files](#orgf92537f)
    1.  [Examples:](#org2a0b6e3)
9.  [modes](#orgfa3b9ec)
    1.  [Markdown-mode](#orgaf3c982)
    2.  [org mode](#org47dafac)
        1.  [lists](#org07a66a2)
        2.  [todo](#org3512bf9)
        3.  [links](#org52ee4a6)
    3.  [typescript mode](#org243bccc)
10. [ibuffer](#orgccd1c2e)
    1.  [Sorting commands:](#org5f163f8)
11. [save keyboard macros](#org059fcd0)

Main **info** folder: 
'C-h i' or 'S-Apple-?'


<a id="orgbd224b8"></a>

# Links/bookmarks

All Emacs manuals: <https://www.gnu.org/software/emacs/manual/>

Choosing Keys to Bind:
<https://www.emacswiki.org/emacs?action=browse;oldid=KeyBindingDiscussion;id=ChoosingKeysToBind>

Elpy CheatSheet:
<https://blog.lobraun.de/2016/03/01/elpy-cheat-sheet/>

Workgroups:
<https://github.com/tlh/workgroups.el>

Collection of video tutorials (
<http://cestlaz.github.io/stories/emacs/>

Elisp Cheatsheet:
<https://github.com/alhassy/ElispCheatSheet/blob/master/CheatSheet.pdf>

Doom emacs
<https://github.com/hlissner/doom-emacs>


<a id="org093c83a"></a>

# Lisp

Emacs Lisp Reference
<https://www.gnu.org/software/emacs/manual/html_mono/elisp.html>

Elisp Cheatsheet:
<https://github.com/alhassy/ElispCheatSheet/blob/master/CheatSheet.pdf>

Overview of text-processing in Emacs lisp
<http://ergoemacs.org/emacs/elisp_editing_basics.html>


<a id="orgffcf852"></a>

# python & elpy


<a id="orgd2d33d5"></a>

# VCS


<a id="orgbd10f0f"></a>

## magit

Magit User Manual: <https://magit.vc/manual/magit.html>

tutorial: <https://magit.vc/screenshots/>

-   magit status: C-x g


<a id="orgaacd7b5"></a>

## vc-diff

C-x v d       directory status
C-x v g       history/blame
C-x v l       log
C-x v m       merge
C-x v u       revert
C-x v x       delete file
C-x v ~       visit different version of file


<a id="org2b7cd91"></a>

## virtual envs

M-x pyvenv-activate
M-x pyvenv-workon (is better)


<a id="orgeefe621"></a>

## elpy

General blog on using elpy(long): <https://realpython.com/emacs-the-best-python-editor/>
Mastering Emacs: <https://www.masteringemacs.org/>

Elpy CheatSheet:
<https://blog.lobraun.de/2016/03/01/elpy-cheat-sheet/>

(elpy-config) activate a virtual env first; use ~/.virtual<sub>envs</sub>/emacs

M-.           goto definition (when it works)
C-d (during auto-complete)
              supposed to give <span class="underline"><span class="underline">doc</span></span>, but doesn't work
M-?           find refs for current identifier


<a id="org6f72618"></a>

### linting

(elpy-check)  run flake8 syntax/style linter
              give C-u arg to get all files in project
	      use C-x \` to move through errors


<a id="org7260f41"></a>

## code-folding

- M-x hs-minor-mode
- M-x hs-hide-all
- M-x hs-show-all
- M-x hs-hide-block
- M-x hs-show-block
- M-x hs-hide\*
- M-x imenu <my<sub>func</sub><sub>name</sub>>
<https://stackoverflow.com/questions/5244485/python-code-folding-in-emacs>

also:
- M-x occur
- M-x fold-dwim and fold-dwim-org


<a id="org21a48f5"></a>

### pyenv

M-x pyenv-mode-set  can be used to set the venv (C-c C-s)


<a id="orgc609313"></a>

# CEDET (Collection of Emacs Development Environment Tools)

<http://cedet.sourceforge.net/>


<a id="orgde0cafb"></a>

# packages:

(package-initialize)
(require 'package)
(setq package-archives '(("gnu" . "<https://elpa.gnu.org/packages/>")
                         ("marmalade" . "<https://marmalade-repo.org/packages/>")
                         ("melpa-stable" . "<https://stable.melpa.org/packages/>")))

(require 'gnutls)
(add-to-list 'gnutls-trustfiles "/usr/local/etc/openssl/cert.pem")


<a id="org0431c31"></a>

## M-x list-packages

provides a buffer all known packages (not just installed); you can install from this list.
If upgrading an already-installed package, you may have to remove the old one from ~/.emacs.d/elpa


<a id="orga6312b9"></a>

## M-x package-refresh-contents


<a id="org0446037"></a>

## M-x package-install <ret> <package-name&#x2026;>


<a id="org1939ea6"></a>

## direx

<https://github.com/m2ym/direx-el>
C-x C-j


<a id="orgbd5b402"></a>

## org-asciidoc

<https://github.com/yashi/org-asciidoc>
org-asciidoc-export-as-asciidoc


<a id="org2b4f6b1"></a>

# yasnippets

Git page: <https://github.com/joaotavora/yasnippet>
Some info: <https://melpa.org/>#
More: <http://cupfullofcode.com/blog/2013/02/26/snippet-expansion-with-yasnippet/index.html>
Better: <https://joaotavora.github.io/yasnippet/snippet-development.html>


<a id="org3588f3a"></a>

## Commands:

create new snippet: M-x yas-new-snippet or C-c & C-n


<a id="orgf92537f"></a>

# editing remote files

<https://stackoverflow.com/questions/20624024/what-is-the-best-way-to-open-remote-files-with-emacs-and-ssh>
/ssh:user@host:/path/to/file

Note: using ssh-copy-id to "export" your id<sub>rsa</sub> and id<sub>rsa.pub</sub> files to the remote machine allows you
to edit files w/o having to enter your password all the time (or to login, or to scp, etc)


<a id="org2a0b6e3"></a>

## Examples:

/ssh:vcassen@dev-gtc-web-003.sea-001.zonarsystems.net:/home/users/vcassen/dbutlerdev/src/gtc/includes/current/showopen.data.php


<a id="orgfa3b9ec"></a>

# modes


<a id="orgaf3c982"></a>

## Markdown-mode

(Note that org-mode is sort of a super-set of markdown mode commands)
see <https://jblevins.org/projects/markdown-mode/>
Markdown cheatsheet: <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>


<a id="org47dafac"></a>

## org mode


<a id="org07a66a2"></a>

### lists

<https://orgmode.org/manual/Plain-lists.html>


<a id="org3512bf9"></a>

### todo

<https://orgmode.org/manual/TODO-items.html>

Rotate TODO status: C-c C-t  (unmarked -> TODO -> DONE)


<a id="org52ee4a6"></a>

### links

[Cheatsheet](https://orgmode.org/manual/External-Links.html)
C-c C-l


<a id="org243bccc"></a>

## typescript mode

Tide (typescript mode)
<https://github.com/ananthakumaran/tide>

ng-mode
<https://github.com/AdamNiederer/ng2-mode>

SO for other TS/an modes
<https://www.reddit.com/r/emacs/comments/dsgi57/can_you_use_emacs_for_angular_2_development/>


<a id="orgccd1c2e"></a>

# ibuffer

<http://doc.endlessparentheses.com/Fun/ibuffer-mode.html>

sort: s 
filter: /   
mark: %     

operate by type: 'n': buffer name, 'f': filename, 'm': mode, etc
(these are the second keystroke after the commands listed above)


<a id="org5f163f8"></a>

## Sorting commands:

',' - Rotate between the various sorting modes.
's i' - Reverse the current sorting order.
's a' - Sort the buffers lexicographically.
's f' - Sort the buffers by the file name.
's v' - Sort the buffers by last viewing time.
's s' - Sort the buffers by size.
's m' - Sort the buffers by major mode.


<a id="org059fcd0"></a>

# save keyboard macros

<https://www.gnu.org/software/emacs/manual/html_node/emacs/Save-Keyboard-Macro.html>

Name last defined macro: C-x C-k n (kmacron-bind-to-key)
Bind last defined macron to key sequence: C-x C-k b ([0-9A-Z] as shortcut to assign key seq C-x C-k n) (don't use "n"!)
Insert macro definition into current buffer: M-x insert-kbd-macro <RET> macroname <RET>

-   Give the above a prefix arg to record keyboard bindings.

