* 0 pykafka vs kafka-python
import pykafka # for pykafka
import kafka   # for kafka-python   <= qs uses this

see http://kafka.apache.org/documentation.html

* 1 Getting Started
"Kafka is a distributed, partioned, replicated commit log service.  It provides the functionality of a messaging system, but iwth a unique design"
see http://kafka.apache.org/documentation.html#introduction

** 0.1 Zookeeper
see https://zookeeper.apache.org/
Zookeeper is a centralized service for maintaining configuration information,
naming, providing distributed synchronization and group services.  It is
used in distributed systems.


Kafka uses zookeeper to handle ?  Config stuff, I think.

** 1.1 Introduction/Core Concepts

- Topics: a feed of messages about some topic
- Producers: emit messages to topics
- Consumers: A process that subscribes to a topic (or topics) and process the messages
- Broker: A server running kafka
- Cluster: One or more brokers

           producer
producer      |       producer
   \------\   |    /-----/
           cluster
   /------/   |   \------\
consumer      |       consumer
           consumer


*** 1.1.1 Topics and Partitions
A topic is a 'name' to which messages are published.  For each topic,
a kafka cluster maintains a partitioned log.  Each partition is an
ordered, immutable sequence.  The unique, sequential id number is
called an "offset".  Offsets are controlled by the consumer; normally
it will just advance its offset sequentially, but it doesn't have to.
Consumers are cheap.

Messages are retained (whether or not they are consumed) for a
configured period of time.

**** Partitions

Some Topic:
Partition 0: [0, 1, 2, 3, ...
Partition 1: [0, 1, 2, 3, 4, 5, ...
Partition 2: [0, 1, ...

Partitions are ordered and immutable.  

For a given topic, partitions might not be on the same server.
Each partition has one "leader" server and zero or more "follower"
servers; the followers passively replicate the leader, and one
takes over of the leader dies.

Partitions serve serveral purposes.
- They allow logs to scale beyond the size of a single server; a
  partition must fit, but not an entire log.
- They act as a unit of parallelism.

Producers choose which partition to write to.


*** 1.1.2 Producers
Producers publish to topic partitions (that is, they choose the
partition to which to publish).

In kafka-python ("kp"), to specify the partition using keyed partitions, use:

producer = KafkaProducer(['some_server:9200'])
future = producer.send('some-topic', b'raw-bytes', key=b'foo')

Note: the number of topic partitions limits the amount of paralell processing possible,
because you can't have more consumers (in a consumer group) than there are partitions.
Therefore, the kafka docs recommends over-partitioning partitions:
(https://cwiki.apache.org/confluence/display/KAFKA/FAQ#FAQ-HowdoIchoosethenumberofpartitionsforatopic?)



Reader Exercises:
- How do you create a topic with multiple partitions?
  1. (manual creation) You can use:
     > bin/kafka-topics.sh --zookeeper zk_host:port/chroot --create --topic <topic-name> --partitions <n-partitions> --replication-factor X --config x=y
  2. (auto creation) You can create topics at runtime; they will be created with
     the number of partitions from the default set in the broker config:
     (see num.partitions in http://kafka.apache.org/documentation.html#brokerconfigs)
     There can also change the number of partitions:
     > bin/kafka-topics.sh --zookeeper zk_host:port/chroot --alter --topic my_topic_name --partitions 40
     (see http://kafka.apache.org/documentation.html#basic_ops_modify_topic)       

*** 1.1.3 Consumers
Rather than using strict queueing (one consumer/message) or publish-subscribe
(many consumers/message), Kafka uses "consumer groups".  Consumers label themselves
as belonging to a consumer group; messages are delivered to one instance within the 
group.  Consumers can be distributed (within one machine or across many).

A consumer group can be thought of as a "logical subscriber".

Reader Exercise:
What happens if there are multiple consumer groups for a single topic?
What happens if there is a single consumer (within a consumer group) for
  a topic that has multiple partitions?

Message ordering: 
Each partition is consumed by exactly one consumer in the consumer group.  This 
allows both load balancing and order guarantees.  (Note: this means there can not
be more consumers than partitions (within one consumer group)).

Guarantees:
- Messasges sent by a producer to a particular topic partition will be appended in the order 
  they are sent.  
- Consumer instance sees messages in the order they are stored in the log.
- For a topic with replication factor N, we will tolerate N-1 server failures
  without loosing any messages.
  

** 1.3 QuickStart:
*** 1.3.1 Download

> tar -xzf kafka_2.10-0.8.2.0.tgz
> cd kafka_2.10-0.8.2.0

*** 1.3.2 Start Zookeeper and Kafka servers

> bin/zookeeper-server-start.sh config/zookeeper.properties &
> bin/kafka-server-start.sh config/server.properties &

*** 1.3.3 Create a topic
Topic named "test", one partition and one replica:

> bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test

Verify with 'list topics':
> bin/kafka-topics.sh --list --zookeeper localhost:2181

*** 1.3.4 Send some messages

Use the following CLI to send one message/line (stdin or file):

> bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
This is a message
This is another message

*** 1.3.5 Start a consumer
> bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic test --from-beginning

*** 1.3.6 Starting a multi-broker cluster (still on a local machine)

> cp config/server.properties config/server-1.properties 
> cp config/server.properties config/server-2.properties

Then edit the broker.id, port, and log.dir fields (just make them different);
broker.id is unique to each cluster
 
Start the nodes:
> bin/kafka-server-start.sh config/server-1.properties &
> bin/kafka-server-start.sh config/server-2.properties &
