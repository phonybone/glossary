ElasticSearch

* Basic Concepts
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_basic_concepts.html
** Cluster:
   A group of nodes (servers) that hold your data and provide federated indexing and
   search capabilities across all nodes.  Id'd by a unique name.

** Node:
   A single server that is part of your cluster.  Id'd by name, default is a Marvel
   comic character ??? (but you can set it).  Nodes can be configured to join specific
   clusters by name (default "elascticsearch").  Unlimited nodes/cluster.

** Index
   Within an index you can define one or more types.  A type is a logical category/
   partition of your index with user-defined semantics.  As an example, given a 
   blogging application, you could store all data in a single index with different
   types for users, blog posts, comments, etc.

** Documents
   Basic unit of information that can be indexed.  Documents are stored using JSON.
   Documents must be assigned a type.

** Shards and Replicas.
   Indexes can store more data than a single server (node) can hold, so data is distributed.
   Subdivide in to shards.  This allows you to:
   - Horizontally scale your content volume;
   - Gain performance via parellelization.
   - Removes single point of failure.

   Mechanics of sharding is transparent to user.
   Replica shards provide failsafe via data replication.

   By default, each ES shard is allocated 5 primary shards and one replica shard.
   Somehow this means if you have two nodes, then you have five primary shards and 
   five replica shards (1 complete replica) for a total of ten shards per index.

* Installation
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html

Java 7 or better
Download a binary via curl
cd elasticsearch
./elasticsearch --cluster.name my_cluster --node my_node

default port = 9200

*****************************************************************************************

** Plugins
*** NLPChina (provides SQL-like interface to ES)
see https://github.com/NLPchina/elasticsearch-sql
see https://github.com/NLPchina/elasticsearch-sql/wiki

elasticsearch-X.Y.Z/bin/plugin install https://github.com/NLPchina/elasticsearch-sql/releases/download/2.1.0/elasticsearch-sql-2.1.0.zip 

*** marvel

*** chrome extension Sense

* REST API
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_exploring_your_cluster.html

** Generic form of HTTP commands:
curl -X<REST Verb> <Node>:<Port>/<Index>/<Type>/<ID>

** Cluster Health
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_cluster_health.html
Use the REST api to make queries as to cluster health.
Can also get a list of nodes in the cluster.

** List Indices
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_list_all_indices.html
curl 'localhost:9200/_cat/indices?v'

** Create Index:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_create_an_index.html
Again, it's an HTTP PUT request to the API:

curl -XPUT 'localhost:9200/customer?pretty'

("pretty" is a parameter to prettify the output)

** Index (ie insert) and query a doc:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_index_and_query_a_document.html

Index a doc:
curl -XPUT 'localhost:9200/customer/external/1?pretty' -d '
{
  "name": "John Doe"
}'

doc type = external, id = 1

Now retrieve it:
curl -XGET 'localhost:9200/customer/external/1?pretty'

Elasticsearch will generate an id for you if you omit it and use POST instead of PUT:
curl -XPOST 'localhost:9200/customer/external?pretty' -d '
{
  "name": "Jane Doe"
}'

** Updating a document:
Elasticsearch does not do in-place updates; rather, it does delete-and-replace operations.
Use POST with an id:
curl -XPOST 'localhost:9200/customer/external/1/_update?pretty' -d '
{
  "doc": { "name": "Jane Doe" }
}'

or 

curl -XPOST 'localhost:9200/customer/external/1/_update?pretty' -d '
{
  "doc": { "name": "Jane Doe", "age": 20 }
}'

** Delete an Index:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_delete_an_index.html
curl -XDELETE 'localhost:9200/customer?pretty'

** Delete a document:
curl -XDELETE 'localhost:9200/customer/external/2?pretty'

** Batch processing:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_batch_processing.html
** Bulk indexing:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_exploring_your_data.html

** Search API:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_the_search_api.html
One uses the '_search' endpoint:

You can put search parameters in to the uri:

curl 'localhost:9200/bank/_search?q=*&pretty'

or the body of the REST request:

curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_all": {} }
}'

You get back JSON containing the results plus some metadata.

ES is stateless; you do your request, get the results back, and that's it.  No server-side
state is retained.

* Query Language (aka "Query DSL")
starting at https://www.elastic.co/guide/en/elasticsearch/reference/current/_introducing_the_query_language.html
see also https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html

** Start with examples:
*** Match all
{
  "query": { "match_all": {} }
}

*** Match anything with pagination and sorting:
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_all": {} },
  "from": 10,
  "size": 10,
  "sort": { "balance": {"order": "desc" }}   # "balance" is a field in the data
}'

*** Request specific doc fields:
Use the "_source" key:

curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_all": {} },
  "_source": ["account_number", "balance"]
}'


*** Match by value:
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match": { "account_number": 20 } }
}'

whereas this matches records where the "address" field contains the string "mill":
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match": { "address": "mill" } }
}'

*** Match by value, using OR:
This will match any record that has "mill" or "lane" in the address:
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match": { "address": "mill lane" } }   # note two values; query will match either of them
}'

*** Match by phrase:
Whereas in this query, the entire phrase "mill lane" must be matched:
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_phrase": { "address": "mill lane" } }
}'

*** Boolean composition of queries: AND, OR
This creates a boolean query where "mill" AND "lane" must be contained within the "address" field.
The "bool" key plus "must" operates as AND:
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}'

Whereas this query matches any record with "mill" OR "lane" in the "address":
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "should": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}'

This is (probably) identical to a query above, but shows the structure of a more complicated OR query.

Here's NEITHER: (NOT AND NOT)
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must_not": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}'

All of this is composable.  Here's a query for age > 40 and doesn't live in ID(aho):
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": [
        { "match": { "age": "40" } }
      ],
      "must_not": [
        { "match": { "state": "ID" } }
      ]
    }
  }
}'


Here's a sense query that matches two fields of lldpneighbors, limiting the size of the response:
GET ethernetinterfaces/_search
{
  "query": {
    "bool": {
      "must": {
        "match": {
          "ladr_metadata.device_uuid": "86f569e7-53a0-4d30-b541-4d8e654ef15d"
        }
      },
      "must": {
        "match": {
          "switchport_mode": "trunking"
        }
      }
      
    }
  },
  "size": 1024
}

Here's the same thing in the style shown at the top of this section:
GET ethernetinterfaces/_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "ladr_metadata.device_uuid": "86f569e7-53a0-4d30-b541-4d8e654ef15d"}},
        { "match": { "switchport_mode": "trunking" }}
        ]
    }
  },
  "size": 1024
}
*** Filters
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_executing_filters.html
**** document _score
_score is a return field that measures relative value of how well the document matches our query.
Not all queries produce _score; they are only produced when necessary (?).

'bool query' (see above) suppert the "filter" key.  For example, here's a filter used
in conjunction with a "range" query (note use of "must": "match_all", then "filter" within that):

**** filter
curl -XPOST 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": { "match_all": {} },
      "filter": {
        "range": {
          "balance": {
            "gte": 20000,   # match all where balance between 20000 and 30000
            "lte": 30000
          }
        }
      }
    }
  }
}'

**** Aggregations
see https://www.elastic.co/guide/en/elasticsearch/reference/current/_executing_aggregations.html
Aggregations provide the ability to group and extract statistics from your data.  Sort of equivalent
to GROUP BY in SQL.




** Reference
see  https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html

The Query DSL is based on JSON and can be thought of an Abstract Syntax Tree (AST)
with leaf query clauses and compound query clauses.

*** Query context vs. Filter context
https://www.elastic.co/guide/en/elasticsearch/reference/current/query-filter-context.html

Query context: "how well does this doc match this query clause?".  A _score is calculated.
Query context is in effect whenever a query clause is passed to one of the query params.

Filter context: "does this doc match this query clause?"  yes or no
In effect whenever a query clause is passed to on of the filter params.

In this example, both query and filter contexts operate, even though the filter clause
is nested within the query clause:

GET _search
{
  "query": { 
    "bool": { 
      "must": [
        { "match": { "title":   "Search"        }}, 
        { "match": { "content": "Elasticsearch" }}  
      ],
      "filter": [ 
        { "term":  { "status": "published" }}, 
        { "range": { "publish_date": { "gte": "2015-01-01" }}} 
      ]
    }   # matches "bool":
  }    # matches "query":
}    


*** match_all
https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-all-query.html
matches every doc.  Used why???

*** full text queries
https://www.elastic.co/guide/en/elasticsearch/reference/current/full-text-queries.html
match: full text match
There are three types: 
- boolean (default):
'operator' defaults to 'or' (can be 'and')
analyzers are used (and can be specified)
'lenient' allows type-mismatches
'fuzziness' allows fuzzy matching


- phrase
- phrase_prefix

multi_match: full text match on multiple fields
common_terms: emphasizes uncommon words
query_string: shortcut for experts
simple_query_string: shortcut for mortals




** Query DSL video
Query Context: "How well does this content match the query clause"; often calculates _score.

Filter Context: "Does this document match the query clause"; yes or no, no _score.  Filter
context is invoked under various conditions, including passing "filter" or "must_not" in a 
"bool" clause, and at other times. (unclear).

* Sorting
see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html

** Troubleshooting
- No mapping found for field in order to sort on in ElasticSearch
  http://stackoverflow.com/questions/17051709/no-mapping-found-for-field-in-order-to-sort-on-in-elasticsearch

* Mapping
see https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html

Mapping is the process of defining how a document, and its fields, are stored and indexed.
Examples:
- Which string fields should be treated as full text fields;
- Which fields contain numbers, dates, geolocations, etc. (TYPES);
- whether the values of all fields in documents should be indexed into the catchall _all field;
- the format of dates;
- custom rules for dynamic fields

** Mapping types
Each index has one or more mapping types.  These assign "types" to various fields (eg "user",
"blogpost", "agent", or maybe a LADR type.

Mapping types have:
- meta-fields, including _index, _type, _id, _source;
- fields (aka properties); type "user" might have "title", "name", "ssn", etc.  Fields
  with the same name, within different types, but in the same index must have the same 
  mapping!

Fields have datatypes, which can be 
- a simple type (string, date, boolean, ip, etc);
- a type support by JSON, eg "object" or "nested";
- a specialized type, eg "geo_point" or "completion" (???)

Fields can be mapped dynamically, automatically, by indexing a document.  
see https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic-mapping.html
for dynamic mapping rules.  However, explicit (ie, user-controlled) mapping
can be more accurate/better.  Explicit mapping can be done when the index is created,
or mappings can be added with the API.  However, existing mappings can not be updated,
as that invalidates indexes (instead, create new indexes).

Fields are shared across mapping types.  Fields that:
- have the same name
- are in the same index
- in different mapping types
- map to the same field internally
all must have the same mapping.

The only exceptions are copy_to, dynamic, enabled, ignore_above, include_in_all, and properties.

*** Example
PUT my_index (1)
{
  "mappings": {
    "user": { (2)
      "_all":       { "enabled": false  }, (3)
      "properties": { (4)
        "title":    { "type": "string"  }, (5)
        "name":     { "type": "string"  }, (6)
        "age":      { "type": "integer" }  (7)
      }
    },
    "blogpost": { (8)
      "properties": { (9)
        "title":    { "type": "string"  }, (10)
        "body":     { "type": "string"  }, (11)
        "user_id":  {
          "type":   "string", (12)
          "index":  "not_analyzed"
        },
        "created":  {
          "type":   "date", (13)
          "format": "strict_date_optional_time||epoch_millis"
        }
      }
    }
  }
}

1. creates an index called "my_index"
2. adds mapping type "user"
3. Disable _all (meta-field) for "user"
4. Start fields (ie, properties) for "user"
5, 6, 7. specify fields for "user"
8. adds mapping type "blogpost"
9. Start property list for "blogpost"
10, 11, 12, 13. "blogpost" properties

** Available field datatypes:
see https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html
Pretty much what you'd expect

** Complex types
see https://www.elastic.co/guide/en/elasticsearch/guide/current/complex-core-fields.html

ES will automatcially map nested objects, BUT it flattens them, which destroys relationships.
That is, it doesn't understand inner objects without help.

** Nested objects: theory
see https://www.elastic.co/guide/en/elasticsearch/guide/current/nested-objects.html
and https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html for basic info

Say we want to index this object:
PUT /my_index/blogpost/1
{
  "title": "Nest eggs",
  "body":  "Making your money work...",
  "tags":  [ "cash", "shares" ],
  "comments": [ 
    {
      "name":    "John Smith",
      "comment": "Great article",
      "age":     28,
      "stars":   4,
      "date":    "2014-09-01"
    },
    {
      "name":    "Alice White",
      "comment": "More like this please",
      "age":     31,
      "stars":   5,
      "date":    "2014-10-22"
    }
  ]
}

Note that it has two comments, with the same structure.  If we rely on dynamic mapping,
the stored document will flatten the fields, sort of like this:

name: ['John', 'Smith', 'Alice', 'White']
comment: ['Great', 'article', 'More', 'like', 'this,', 'please']
age: [28, 31]

and so forth.  The mapping will have type 'object'.

Now, consider a search query like this:
GET /_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "name": "Alice" }},
        { "match": { "age":  28      }} 
      ]
    }
  }
}

What was probably meant was "return all docs with name=="Alice" and age==28", and the doc would match,
even though Alice's age is 31.  Not good.  (see https://www.elastic.co/guide/en/elasticsearch/guide/current/complex-core-fields.html#object-arrays)

To solve this problem, we have to map the 'comments' field as 'nested'.  Now each nested field is 
indexed as a /hidden separate/ document, sort of like this:

{ 
  "comments.name":    [ john, smith ],
  "comments.comment": [ article, great ],
  "comments.age":     [ 28 ],
  "comments.stars":   [ 4 ],
  "comments.date":    [ 2014-09-01 ]
}
{ 
  "comments.name":    [ alice, white ],
  "comments.comment": [ like, more, please, this ],
  "comments.age":     [ 31 ],
  "comments.stars":   [ 5 ],
  "comments.date":    [ 2014-10-22 ]
}
{ 
  "title":            [ eggs, nest ],
  "body":             [ making, money, work, your ],
  "tags":             [ cash, shares ]
}

As hidden documents, they cannot be directly accessed.  To modify a hidden doc, the entire
doc must be re-indexed, and search queries return then entire doc as well.

So 'nested' applies to lists (nested within the main doc) of like structure, where we want to 
keep the individual "sub-documents" (i.e. list elements) separate for purposes of searching.
Without nesting, the doc would basically merge (flatten) all the list elements together, sort of.

** Nested objects: practice
The basic rule is that whenever you want arrays of nested objects,
you use the key 'nested' in place of 'object' in the mapping dict:

PUT /my_index
{
  "mappings": {
    "blogpost": {
      "properties": {
        "comments": {
          "type": "nested",        <===== RIGHT HERE!
          "properties": {
            "name":    { "type": "string"  },
            "comment": { "type": "string"  },
            "age":     { "type": "short"   },
            "stars":   { "type": "short"   },
            "date":    { "type": "date"    }
          }
        }
      }
    }
  }
}

Note how 'blogposts' has 'comments' (and presumably other missing properties?),
and the type of 'comments' is 'nested'.  It is then followed with its own
'properties' dict.

see https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html for 
basic info on nested types.

** Searching nested objects:
see http://joelabrahamsson.com/elasticsearch-nested-mapping-and-filter/ for a
discussion of when to use nested *queries* on nested types.

** Parent-child mapping
see https://www.elastic.co/guide/en/elasticsearch/guide/current/parent-child.html

*** Parent-child vs nested
In nested mappings, an entire tree is stored as one document, and while you can 
search on nested keys, the entire doc is returned.  With parent-child mapping, 
all documents are stored separately, which means it can be both searched and 
updated on its own.  

One limitation of parent-child mapping is that the entire document must live
on the same shard.

*** creating the mapping
Consider the situation where we have a company with employees who work in 
different branches.  We'd like to associate the employee with their branch.
Since we'd like to search for branches, employees, and employees that work
for specific branches, so the nested mapping is no good (ok..?).

To implement a parent-child relationship between branches and employees,
we specify the '_parent' field in the employee mapping:

PUT /company
{
  "mappings": {
    "branch": {},
    "employee": {
      "_parent": {
        "type": "branch" 
      }
    }
  }
}

This does appear to limit the parent of employees to branch, precluding other
parents (such as 'boss').

*** Indexing parents and children:
see https://www.elastic.co/guide/en/elasticsearch/guide/current/indexing-parent-child.html
Indexing parents is basically a no-op.
When indexing children, you have to include the id of the parent doc in the url.
For grandparent relationships, you have to use a routing parameter

*** Finding parents by their children
see https://www.elastic.co/guide/en/elasticsearch/guide/current/has-child.html

*** Various links on parent-child vs nesting:
https://discuss.elastic.co/t/questions-about-mapping-a-tree-structure/13463

** Default Mappings
https://www.elastic.co/guide/en/elasticsearch/guide/current/default-mapping.html
Used when lots of types in an index share a definition (fields and settings).
(Note that doc_sevice/mappings/__init__.py defines a default mapping that gets
included in each ladr_type's mapping).

_default_mappings are also a good place for index-wide dynamic-templates.

** Dynamic Templates
https://www.elastic.co/guide/en/elasticsearch/reference/current/dynamic-templates.html
Allows you to specify custom rules for dynamic mapping based on field name, field path,
or data type.

** doc service


* Indicies
** Index Aliases
see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-aliases.html
Aliases can represent one (or more) indexes, and all the ES APIs will do the right translation.
(But you can't insert into an alias that points to more than one index).  You can even
use globs to point to many indices.

*** Filtered Aliases
Aliases can also point to filters (?) and applied when searching and routing.  These are
akin to "views".  You create a filter on one (or more) of the fields in the index.  Effect?
The filter is defined using the Query DSL and applied to searches, counts, delete by query, 
and "more like this" opertions.

*** Routing
Aliases can also be applied to routing to avoid unnecessary shard operations.  
Useful for parent-child searches?

* Modeling Relationships
see https://www.elastic.co/guide/en/elasticsearch/guide/current/relations.html to start
tl;dr NoSQL models the world as if it were flat, but it isn't.  There are advantages to 
flat mapping, but we still have to handle relationships.  There are various strategies
to do so:
** Application-side joins
see https://www.elastic.co/guide/en/elasticsearch/guide/current/application-joins.html
Trades single source of truth for all docs (ie, exactly one copy of data) for multiple
queries and application-side building of results.
** Denormalization
see https://www.elastic.co/guide/en/elasticsearch/guide/current/denormalization.html
This is pretty much how elasticsearch is intended to be used.  It involves having 
redundant copies of your data in every record.  The upside is no joins, for faster
performance (fewer queries).  The downside is increased disk usage).

Disadvantage: what happens when a piece of data is changed?  Then every document that
contains a copy of that data must be changed.
** Field Collapsing


* SQL plugin
see https://github.com/NLPchina/elasticsearch-sql/wiki
** Nested Queries
see https://github.com/NLPchina/elasticsearch-sql/wiki/NestedTypes-queries
** Aggregations
see Stateless/

This works:
SELECT name, ladr_metadata.updated FROM vlans 
where ladr_metadata.updated <= "2015-12-11 18:14:42.999999"
order by (ladr_metadata.updated)
limit 1

** Scripting
see https://github.com/NLPchina/elasticsearch-sql/wiki/Script-Fields
* Modeling Time
see https://www.elastic.co/guide/en/elasticsearch/guide/current/time-based.html
* Scripting (aka StoredProcedures)
see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-script-fields.html

* Python client:
see https://elasticsearch-py.readthedocs.org/en/master/
** Api
see https://elasticsearch-py.readthedocs.org/en/master/api.html

** Elasticsearch object:
