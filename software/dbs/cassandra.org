* Term definitions
** column family:
   see http://docs.datastax.com/en/getting_started/doc/getting_started/keyConDataMdl.html?scroll=keyConDataMdl__data-model
   A set of key-value pairs.  Each column family has a key and consists of columns and rows.
   In general, analogous to a traditional db table (sorta); "table" replaces "column family"
   in recent tools (eg CQL 3). 
   Columns are ordered.

** gossip protocol
   the communication protocol by which cassandra shares data amongst nodes.
   http://docs.datastax.com/en/cassandra/3.0/cassandra/architecture/archGossipAbout.html

** Keyspace
   Outermost grouping of data; contains tables (aka "column families").  Similar to a dataase
   in traditional parlance.  Typically one keyspace/application.

* Some useful cassandra cli commands
** help;
** show keyspaces;
Shows all top-level containers
** describe keyspace <ks>
lists all column families in keyspace
** list <cf>
get actual rows of a column family
** get <cf>['<key>']
get value (indexed by <key>) from a specific cf; kind of like 
"SELECT * FROM <cf> WHERE primary_key='<key>';"


* Architecture:
see http://docs.datastax.com/en/cassandra/3.0/cassandra/architecture/archIntro.html

Meant to handle big workloads across multiple nodes with no SPOF.
Distributes and replicates data.  Data is exchanged across nodes via
the "gossip" protocol.  A seq- uentially written "commit log" captures
all local data writes (to ensure data durability).  Data is then
indexed in a memory structure called a "memtable" (similar to a
write-back cache).  When a memtable is full, it is written to disk in
an SSTable file.  SSTables are immutable.  All writes are replicated
throughout a cluster of nodes.  Periodically SSTables are "compacted",
where data marked for deletion with a "tombstone" is actually removed.
Finally, repair mechanisms exist to ensure data consistency.

Cassandra is a "partitioned row store" database.  Rows are organized in to tables with
a required primary key.

CQL is used to access the data, via cqlsh, the DevCenter (web interface), or by drivers
for various application languages.

Client r/w requests can be sent to any node in a cluster; the connected node serves as a
coordinator for the client operation, and manages requests from that client.

* Database internals
see http://docs.datastax.com/en/cassandra/3.0/cassandra/dml/dmlDatabaseInternalsTOC.html
** Storage Engine
Cassandra uses a "log-structured merge tree" (see wikipedia) (this is
the memtable?).  This allows cassandra to avoid "read before write",
which is a complication in distributed data systems.  Data to be
written is grouped (how?) and only updates are (sequentially) written.
Cassandra never re-writes or re-reads existing data, and never
overwrites rows in place.  (Immutability FTW).  Sequential writes are
also a big performance win.

** Data writes
http://docs.datastax.com/en/cassandra/3.0/cassandra/dml/dmlHowDataWritten.html
- appends data to commit log
- writes to memtable
- when memtable full, flush to sstable.  Sort memtable by token (primary key?),
  then sequential write to disk.  A partition table is also created that maps
  tokens to disk locations.

For each SSTable, three structures are created:
- partition index: maps partition key to disk location
- partition summary: a sample of the partition index, stored in memory
- bloom filter: a memory structure that checks if row data exists in the
  memtable before accessing sstable (on disk)

** Data Maintenance (Compaction)
see http://docs.datastax.com/en/cassandra/3.0/cassandra/dml/dmlHowDataMaintain.html
Because cassandra does not insert/update in place, and because SSTables are immutable,
data tables must be periodically compacted.  

Immutability implies that a new SSTable is written with the latest version of some data,
while the old versions still exist.  Hence multiple versions of data may exist, each with 
a different set of stored columns.  As SSTables accumulate, they must all be read to in 
order to retrieve an entire row.  

Compaction merges the data in the SSTables by selecting the latest version based on 
the timestamp.  Because rows are sorted by partition key within each sstable, it's 
fast (no random io).  Once old sstables are compacted/merged, they're deleted.  (After
cassandra 2.1, various optimizations are made that allow reading while compaction is 
in progress.

There are various compaction strategies, with various optimized use cases.  See link.

** Data Reads
Reads must consider the active memtable and multiple sstables.  Process is as follows:
*** check memtable
    if data found, combine with sstable and return
*** if row caching enabled, check row cache
    The row cache stores a subset of the partition data (sstables) stored on disk.
    Stores most accessed data, as entire rows, with LRU eviction policy.  (Note
    that if data request spans rows, it can't be cached?).  Row cache size is
    configurable, as is number of rows to cache.

*** check bloom filter
    If row cache is a miss, check bloom filter.  This indicates which sstables are
    likely to contain the data.  Bloom filters can determine if the sstable does
    not contain requested data, but not that the sstable does contain (only a 
    probability).  If the bloom filter does not rule out an sstable, then the
    partition key cache is checked (if enabled).  (One bloom filter/sstable).

    Bloom filters can trade memory for performance.
    
*** if partition key caching enabled, check Partition Summary
    Stores a cache of the partition index in (off-heap) memory.  Hits lead to 
    compression offset map to find compressed block on disk.  On misses, the 
    partition summary must be consulted.

*** if partition key is found (in cache), consult compression offset map
*** else, check partition summary
    Partition summary is also off-heap memory.  The partition summary maps
    every Xth partition key's location in the index file.  After finding 
    the range of partition key values, the partition index is searched.

    This is another memory-for-performance optimization.
    
*** Partition Index
    Resides on disk.  Maps all partition keys to their offset.

*** locate data using compaction offset map, then fetch.
    The compaction (compression) offset map contains pointers to exact
    disk locations of a given partition.  It resides in off-heap memory and
    is accessed by both partition key cache and partition index (as needed, above).
    

* Python driver (client)
** getting started
http://datastax.github.io/python-driver/getting_started.html


* Tutorials
** Cassandra Basics: 
*** Introduction
https://www.morsecodist.io/programming/2019/7/7/cassandra-basics-introduction
*** Primary Keys
https://www.morsecodist.io/programming/2019/7/7/cassandra-basics-primary-keys

** Time Series Data: getting started
see https://academy.datastax.com/demos/getting-started-time-series-data-modeling
The tutorial discusses modeling time-series temperature data from a weather station
with one measurement per minute.

*** Pattern 1: single row / station
Uses the station id as a row key.
The timestamp is the column name, the temp reading is the (single) column value.
The row grows as needed.

CREATE TABLE temperature (
  weatherstation_id text,
  event_time timestamp,
  temperature text,
  PRIMARY KEY (weatherstation_id,event_time)
);

see http://stackoverflow.com/questions/24949676/difference-between-partition-key-composite-key-and-clustering-key-in-cassandra
for an explantion of different key types in cassandra.
also http://docs.datastax.com/en/cql/3.0/cql/cql_reference/create_table_r.html

Insert values:

INSERT INTO temperature(weatherstation_id,event_time,temperature) VALUES (’1234ABCD’,’2013-04-03 07:01:00′,’72F’);

And retrieve it:

SELECT event_time,temperature FROM temperature WHERE weatherstation_id=’1234ABCD’;
or
SELECT temperature
  FROM temperature
  WHERE weatherstation_id=’1234ABCD’
  AND event_time > ’2013-04-03 07:01:00′
  AND event_time < ’2013-04-03 07:04:00′;

*** Pattern 2: Partitioning to limit row size
Cassandra can store up to 2 billion columns/row.  Sometimes, that's not enough.

Row partitioning adds data to the row key to limit the data/row.  For example, appending
the date to the station id causes only one day's worth of data to be stored to a single row:

PRIMARY KEY ((weatherstation_id,date),event_time)

Inserts now have an additional field:

INSERT INTO temperature_by_day(weatherstation_id,date,event_time,temperature)
VALUES (’1234ABCD’,’2013-04-03′,’2013-04-03 07:01:00′,’72F’);

*** Pattern 3: reverse order time series with expiring columns
Add some semantics to the CREATE TABLE statement:

PRIMARY KEY (weatherstation_id,event_time),
) WITH CLUSTERING ORDER BY (event_time DESC);

Then provide a TTL arg to INSERT:

INSERT INTO latest_temperatures(weatherstation_id,event_time,temperature)
VALUES (’1234ABCD’,’2013-04-03 07:03:00′,’72F’) USING TTL 20;

This removes data older than 20 seconds during compaction.


* Luminus
** Rollup Service writes to cassandra
- creates a table for agent rollups:
  session.execute( "CREATE TABLE agent_rollups ( \
                    agent_uuid text, \
                    update_data text, \
                    update_time timestamp, \
                    update_topic_offset bigint, \
                    PRIMARY KEY (agent_uuid,update_time), \
                  ) WITH CLUSTERING ORDER BY (update_time DESC);" )

- writes occur in _write_back(self).  rollup_data is pulled off the WriteBackQueue,
  stored in to the table above.  Note that data is (cluster) ordered by update_time.

** Data Service:
- queries data inserted by rollup service.  see load_agent_data().
  query is "select distinct agent_uuid from agent_rollups;", then
           "SELECT update_data FROM agent_rollups WHERE agent_uuid = ? and update_time < ? limit 1"
