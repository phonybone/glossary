Elasticsearch logstash kibana (Elk) tutorial (and logstash forwarder)
see https://www.digitalocean.com/community/tutorials/how-to-use-logstash-and-kibana-to-centralize-and-visualize-logs-on-ubuntu-14-04

Logstash: the server component (of Logstash?) that processes incoming logs
ElasticSearch: Log store
Kibana: visualization
LogstashForwarder: Installed on servers that send logs to logstash; uses lumberjack protocol.

First three (elk) get installed on one server/host; logstash forwarder gets installed
on all servers that generate logs.

ES and LS require Java7

* Installation tutorial (digital ocean)
** Install ElasticSearch
wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
echo 'deb http://packages.elasticsearch.org/elasticsearch/1.1/debian stable main' | sudo tee /etc/apt/sources.list.d/elasticsearch.list
sudo apt-get update
sudo apt-get -y install elasticsearch=1.1.1
# edit config:
sudo vi /etc/elasticsearch/elasticsearch.yml
# add this line:
edit> script.disable_dynamic: true
# uncomment this line:
edit> network.host: localhost
sudo service elasticsearch restart

# start ES on boot:
sudo update-rc.d elasticsearch defaults 95 10

** Install Kibana:
cd ~; wget https://download.elasticsearch.org/kibana/kibana/kibana-3.0.1.tar.gz
tar xvf kibana-3.0.1.tar.gz
sudo vi ~/kibana-3.0.1/config.js
# replace port 9200 with port 80
edit>    elasticsearch: "http://"\+window.location.hostname\+":80",
 
We use nginx to serve kibana, so:
sudo mkdir -p /var/www/kibana
sudo cp -R ~/kibana-3.0.1/* /var/www/kibana3/
sudo apt-get install nginx

Need to configure nginx to proxy port 80 to port 9200.  Kibana provides an nginx config:
cd ~; wget https://gist.githubusercontent.com/thisismitch/2205786838a6a5d61f55/raw/f91e06198a7c455925f6e3099e3ea7c186d0b263/nginx.conf
vi nginx.conf
edit> server_name FQDN; (or localhost)
edit> root /var/www/kibana
sudo cp nginx.conf /etc/nginx/sites-available/default

#  install apache2-utils so we can use htpasswd to generate a username and password pair:
sudo apt-get install apache2-utils
sudo htpasswd -c /etc/nginx/conf.d/kibana.myhost.org.htpasswd <username>
sudo service nginx restart

** Install LogStash
# create the Logstash source list:
echo 'deb http://packages.elasticsearch.org/logstash/1.4/debian stable main' | sudo tee /etc/apt/sources.list.d/logstash.list
sudo apt-get update
sudo apt-get install logstash=1.4.2-1-2c0f5a1

# Generate SSL certs:
sudo mkdir -p /etc/pki/tls/certs
sudo mkdir /etc/pki/tls/private

# Two options, depending if you're running DNS to your LS server or not.  If yes, use Opt2
*** Option 1: Add Logstash IP to SSL cert:
sudo vi /etc/ssl/openssl.cnf
# below "[ v3_ca ]"
edit> subjectAltName = IP: <logstash_server_private_ip>

cd /etc/pki/tls
sudo openssl req -config /etc/ssl/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt

*** Options 2
# create an A record that contains the Logstash Server's private IP address
cd /etc/pki/tls 
sudo openssl req -subj '/CN=logstash_server_fqdn/' -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt

# Configure LogStash
Logstash configs are in /etc/logstash/conf and are written in JSON:

# specify a lumberjack input listening on port 5000 and using the ssl cert generated above:
sudo vi /etc/logstash/conf.d/01-lumberjack-input.conf
edit> input {
  lumberjack {
    port => 5000
    type => "logs"
    ssl_certificate => "/etc/pki/tls/certs/logstash-forwarder.crt"
    ssl_key => "/etc/pki/tls/private/logstash-forwarder.key"
  }
}

# listen to syslog:
sudo vi /etc/logstash/conf.d/10-syslog.conf
edit> filter {
  if [type] == "syslog" {
    grok {
      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]
    }
    syslog_pri { }
    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
  }
}

# configure Logstash to store the logs in Elasticsearch:
sudo vi /etc/logstash/conf.d/30-lumberjack-output.conf
edit> output {
  elasticsearch { host => localhost }
  stdout { codec => rubydebug }
}

If you want to add filters for other applications that use the
Logstash Forwarder input, be sure to name the files so they sort
between the input and the output configuration (i.e. between 01 and
30).

sudo service logstash restart

** Install LogstashForwarder
Do these steps on each server.

# On logstash server, copy ssl cert to log-generating server:
scp /etc/pki/tls/certs/logstash-forwarder.crt <user@server_private_IP>:/tmp

# On server, create the Logstash Forwarder source list and install logstash-forwarder:
echo 'deb http://packages.elasticsearch.org/logstashforwarder/debian stable main' | sudo tee /etc/apt/sources.list.d/logstashforwarder.list
wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get update
sudo apt-get install logstash-forwarder

# install the Logstash Forwarder init script, so it starts on bootup:
cd /etc/init.d
sudo wget https://raw.githubusercontent.com/elasticsearch/logstash-forwarder/a73e1cb7e43c6de97050912b5bb35910c0f8d0da/logstash-forwarder.init -O logstash-forwarder
sudo chmod +x logstash-forwarder
sudo update-rc.d logstash-forwarder defaults

# copy ssl certs:
sudo mkdir -p /etc/pki/tls/certs
sudo cp /tmp/logstash-forwarder.crt /etc/pki/tls/certs/

# configure logstash forwarder on servers:
sudo vi /etc/logstash-forwarder
edit> {
  "network": {
    "servers": [ "<logstash_server_private_IP:5000>" ],
    "timeout": 15,
    "ssl ca": "/etc/pki/tls/certs/logstash-forwarder.crt"
  },
  "files": [
    {
      "paths": [
        "/var/log/syslog",
        "/var/log/auth.log"
       ],
      "fields": { "type": "syslog" }
    }
   ]
}

sudo service logstash-forwarder restart

** Connect to kibana
Go to kibana site with browser (localhost or where kibana is running).
There is a screen prompting you to configure an index pattern (must have at 
least one).  "Index patterns are used to identify the ElasticSearch index
to run search and analytics against.  They are also used to configure fields."

Select @timestamp from dd menu and click Create, then click Discover


* Luminus ELK installation:
Git repos:
https://github.com/luminusnetworks/logstash-forwarder
https://github.com/luminusnetworks/elk

Each runs a server in a docker container.  
Each has various files under github control that then get loaded
into the docker image via the Dockerfile.
** ELK



* ElastAlert
see https://elastalert.readthedocs.org/en/latest/elastalert.html

ElastAlert works by combining ElasticSearch with two types of components: rule (types) and alerts.
ES is polled and data is passed to a rule type, which determines when a match is found.  When
a match is found, it is given to one or more alerts, which performs some action.

All this is configured by a set of rules, each of which defines a query, a rule type, and a set of alerts.

Several rule_types are included with EA.  You can also write your own.

** Luminus Installation
The relevent git repo is 'event_service'.  It defines elasticalert configs and implements a
rule and an alert.  It also implements a service running in a docker container, but I'm not sure
why...

We define one rule_type (AgentUpdateFrequenceRule) and one alert (SlackAlerter).  We also use a 
built-in rule_type "frequency" that also uses the same SlackAlerter.



*** AgentUpdateFrequencyRule
- derives from elastalert.ruletypes.SpikeRule
- overrides(?) get_match_str(self, match) to create a message

*** SlackAlerter
- grabs some values from the config (self.url -> self.rule['slack_url']
- refers to self.rule and elastalert.alerts.BasicMatchString to create message to send to slack
- uses requests.post to send a message to slack

