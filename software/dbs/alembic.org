SQLA/Alembic cookbook: https://alembic.sqlalchemy.org/en/latest/cookbook.html
[[https://alembic.sqlalchemy.org/en/latest/ops.html][Operations Reference]]

* Getting started:
pip install alembic
alembic init alembic
alembic revision -m 'create first table'



* Create a migration script/step
see http://alembic.zzzcomputing.com/en/latest/tutorial.html#create-a-migration-script

alembic revision -m "create account table"
alembic upgrade head

** Auto-generate a migration by inspecting an existing db:
https://alembic.sqlalchemy.org/en/latest/autogenerate.html

* Run raw sql in migration script
https://stackoverflow.com/questions/17972020/how-to-execute-raw-sql-in-flask-sqlalchemy-app

** example of SELECT:
# taken from routeboard-db/alembic/versions/a9c11cc21ad8_convert_onroute_status_to_string.py (no longer included)
 conn = op.get_bind()
 results = conn.execute("SELECT status FROM onroute GROUP BY status").fetchall()
 print(F"results {results}")
 old_values = [val if val in ok_values else 'pending' for val in list(results)]
 print(F"old_values: {old_values}")
 old_values = list(set(old_values).union(ok_values))
 print(F"old_values: {old_values}")


* Problem with DRS ETL & alembic
Since we commonly destroy the entire DB in development (via run_local.sh), does the alembic_version 
table get killed?  We call `alembic upgrade head` directly after the calls to destroy/create the db,
so it should be there (it is).

Whatever the reason, there seems to be times when the alembic_version table gets deleted...

Also, why does `alembic upgrade head`
