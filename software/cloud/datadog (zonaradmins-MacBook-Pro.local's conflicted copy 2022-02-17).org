* API and Application Keys
https://docs.datadoghq.com/account_management/api-app-keys/
also https://app.datadoghq.com/account/settings#api ?
API key is organization-wide.
Applications keys give you access to DD's programmatic API.  They are 
associated with a user account and have names.  I'm not sure where/how
Zonar uses them.

Zonar: f2d4b8a0049ae7e7ff83e3a497420b3a
Chris': ca925cc7ca206e1682e6786b723dc616f446e31b

* Agent & Config
https://docs.datadoghq.com/agent/

Installation: https://app.datadoghq.com/account/settings#agent/mac

Mac OS X
https://docs.datadoghq.com/agent/basic_agent_usage/osx/?tab=agentv6v7

start: launchctl start com.datadoghq.agent
stop: launchctl stop com.datadoghq.agent
(also, see 'bone' icon in systray (top of screen next to bluetooth or whatevs)

status: datadog-agent status

/usr/local/bin/datadog-agent

  file:~/.datadog-agent/datadog.yaml

DD needs to run an agent in order to report logs, traces, whatever.  When
running a program on localhost, you also run the agent as a service.  When
running on k8, you apparently run it on a VM/pod within your cluster, and 
tell the deployment where the host is using an env var "DD_AGENT_SERVICE_HOST"
(at least that is how DRS does it, in it's deployment.yaml).

Account Settings (incl API keys) https://app.datadoghq.com/account/settings#api

** Docker Agent:
https://docs.datadoghq.com/agent/docker/?tab=standard

docker-compose:
https://docs.datadoghq.com/integrations/faq/compose-and-the-datadog-agent

** K8 Agent
https://docs.datadoghq.com/agent/kubernetes/
https://docs.datadoghq.com/agent/kubernetes/host_setup/
Using Daemonset
https://docs.datadoghq.com/agent/kubernetes/daemonset_setup/?tab=k8sfile

* Integrations
DD can send info from hundreds of different software packages, from dbs to message Qs to 
cloud platforms (incl k8).

** Postgres
https://docs.datadoghq.com/integrations/postgres/?tab=host

* Metrics
** Custom Metrics
see https://docs.datadoghq.com/developers/dogstatsd/?tab=python
Uses statsd protocol, and DogStatD (bundled with DD agent).  DogStatsD implements StatsD
protocol with DD-specific extensions.

Datadog-specific extensions:
- Histogram
- Service Checks
- Events
- Tagging

Uses UDP between your app and DogStatsd, then HTTPS between DogStatsD and app.datadoghq.com.
Flush interval is 10 secs by default.  Uses port 8125 by default.

To send custom metrics in python, do:
> pip install datadog
'''
import datadog

options = {
    'statsd_host':'127.0.0.1',
    'statsd_port':8125
}

datadog.initialize(**options)
'''



* Events
https://docs.datadoghq.com/events/

* Logs
Can get logs into dd via an agent

There is a [logs] section in the datadog.yml
file://opt/datadog-agent/etc/datadog.yaml

You can also collect via Docker containers and k8

** Tailing a local log file
see https://docs.datadoghq.com/getting_started/logs/?tab=ussite
and https://docs.datadoghq.com/getting_started/logs/?tab=ussite#monitor-a-custom-file

- Local agent has to be installed and running and configured.
-- Need to create a new folder and populate w/conf.yaml:
touch -d  ~/.datadog-agent/conf.d/trigger_etl_metrics.d/conf.yaml

conf.yaml example:
```
init_config:

instances:

logs:
  - type: file
    path: /Users/victor.cassen/Dropbox/Zonar/sandbox/pupil_ingestion_service.log
    service: pupil_ingestion_service
    source: python
  - type: file
    path: /Users/victor.cassen/Dropbox/Zonar/sandbox/daily_route_service.log
    service: daily_route_service
    source: python
```    
Note: the name of the folder holding conf.yaml has to end in '.d'
Note: the script that is writing these logs is ~/Dropbox/Zonar/sandbox/trigger_etl_metrics.py
Note: Don't forget to restart the dd agent (on Mac, can use systray)
Note: `datadog-agent status` should return a section pertaining to the above configuration, as seen below:
'''
Logs Agent
==========
    Sending uncompressed logs in SSL encrypted TCP to agent-intake.logs.datadoghq.com on port 10516
    BytesSent: 1010
    EncodedBytesSent: 1010
    LogsProcessed: 3
    LogsSent: 3

  trigger_etl_metrics
  ---------------
    Type: file
    Path: /Users/victor.cassen/Dropbox/Zonar/sandbox/daily_route_service.log
    Status: OK
    Inputs: /Users/victor.cassen/Dropbox/Zonar/sandbox/daily_route_service.log 
'''
Note: for some reason, can only see these logs using 'Live Tail'.  It has something to do with 
indexing; you can see them in the 'main' index.

** Indexing
Zonar's indexes are at: https://app.datadoghq.com/logs/pipelines/indexes
The pupil-index currently reads from the kube_namespace pupil-dev OR pupil-staging OR pupil-prod, and excludes DEBUG logs for various services.

* APM
[[https://docs.datadoghq.com/tracing/setup_overview/setup/python/][APM Setup]]

** Watchdog
https://docs.datadoghq.com/watchdog/
Watchdog is an algorithmic feature for APM performances and
infrastructure metrics that automatically detects potential
application and infrastructure issues.

** RTB-API /image/student 
env:development service:routeboard-api operation_name:flask.request resource_name:get_image
DD APM page https://app.datadoghq.com/apm/resource/routeboard-api/flask.request/cec32bb4a41e022 (might have to adjust time span)
* Monitoring and Alerting
** Dashboards
[[https://docs.datadoghq.com/dashboards/][Dashboard Overview]]
[[https://docs.datadoghq.com/dashboards/template_variables/][Template Variables]]

** snippets
*** conditional alerting:
Got this snippet from Daniel, not 100% sure where to apply it...

{{^is_match "environment.name" "production"}}
   {{^is_match "environment.name" "uat"}}
      {{#is_alert}} @slack-datadog {{/is_alert}}
   {{/is_match}}
{{/is_match}}
** monitoring cloud functions
https://docs.datadoghq.com/integrations/google_cloud_functions/
* Zonar-specific:
[[https://confluence.zonarsystems.net/pages/viewpage.action?pageId%3D112951486][Tutorial/Guide for GCS on how to add metrics and see them in Datadog]] (Confluence)
[[https://app.datadoghq.com/access/application-keys][Zonar Application Keys]]
[[https://protect-us.mimecast.com/s/dQGcCJ6r4EurvRLhV-hEt?domain%3Dapp.datadoghq.com][Zonar Login Page]]

** Metrics in GCP Cloud Functions
*** To create:
- need to create an app-specific DATADOG_APP_KEY (see link above), pass to datadog.initialize()
- use datadog.ThreadStats, pass in constant_tags (array of ':'-delimited strings)
- set can use .gauge(), .increment(), .timing() on ThreadStats object
- metric names can be anything, but live in organization scope; use tags to filter.

*** To view
[[https://app.datadoghq.com/dashboard/aan-bw3-nn9][OnRoute Datadog Dashboard]]
Or:
- go to metrics explorer
- can have multiple metric graphs displayed; pick in 'Graph:' dropdown
- filter by tags in 'Over:' eg 'service:______' or 'env:development', etc (can have multiple filters)
