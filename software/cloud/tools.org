Various tool to research later as needed

* inletsctl
https://inlets.dev/
 inlets is your Cloud Native tunnel to get a public IP when it counts,
 sharing work with clients, your team, or the community. It's like
 Ngrok, but for 2020, and free

* k3d
curl -s https://raw.githubusercontent.com/rancher/k3d/master/install.sh | bash

to run an entire Kubernetes cluster in a docker container, the fastest
option available and smallest footprint, built on k3s

* k3sup
curl -sSLf https://get.k3sup.dev | sudo sh

k3sup can be used to install k3s on remote VMs, your Raspberry Pi
cluster. The second use-case for k3sup is to install apps like the
Kubernetes Dashboard, Minio, Postgresql, OpenFaaS, cert-manager and
more, using their helm charts.

* kubectx
cd /tmp/
git clone https://github.com/ahmetb/kubectx
chmod +x ./kubectx/kubectx
sudo cp ./kubectx/kubectx /usr/local/bin/

kubectx - kubectx is a bash script which switches quickly between
Kubernetes contexts. You'll need this for pointing at either a local
or a remote cluster.

* hub
hub - if you're a maintainer, or ever test PRs on OSS projects, then you'll want the "Hub" CLI by GitHub.

curl -sSL https://github.com/github/hub/releases/download/v2.14.1/hub-linux-amd64-2.14.1.tgz > /tmp/hub.tgz
sudo tar -xvf /tmp/hub.tgz -C /usr/local/ --strip-components=1

* stack on a budget
"This repository offers a collection of services with great free tiers
for developers on a budget. Because not everyone has 20$ per month to
spend on app or database hosting for every single side-project."

https://github.com/255kb/stack-on-a-budget

Full stack on home hardware: https://github.com/khuedoan/homelab


* asdf
[[https://asdf-vm.com/][Home]]
asdf is a tool version manager. All tool version definitions are
contained within one file (.tool-versions) which you can check in to
your project's Git repository to share with your team, ensuring
everyone is using the exact same versions of tools.

The old way of working required multiple CLI version managers, each
with their distinct API, configurations files and implementation
(e.g. $PATH manipulation, shims, environment variables, etc...). asdf
provides a single interface and configuration file to simplify
development workflows, and can be extended to all tools and runtimes
via a simple plugin interface.

