AMI - Amazon Machine Instance
AWS - Amazon Web Services
EC2 - Elastic Cloud Computing
EBS - Elastic Block Storage
    Persistent independent of EC2 instances; can attach or detach
    Can also be started and stopped
