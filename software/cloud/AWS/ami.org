Notes regarding Amazon Machine Images (AMI's)

* command line: ec2-create-image

* Instance must be stopped to create an AMI from it.
* EBS volumes must be detached? 
** Not exactly; the image will include the EBS volume if it is still attached.
* EIP's get disassociated automatically

* Create script /etc/init.d/ec2 to run provisioning commands at boot-time
** See example in Kindle book
** Need to have EC2 command-line tools installed
** Do `update-rc.d ec2 defaults` to insure script gets run (ubuntu)
* Can store AMIs with EBS or S3 (aka "store-backed"), but EBS now prefered.

* Instance Store (S3):
* EBS-backed instance
** Has one mandatory EBS Root Device Volume
** Can have additional EBS Volumes
** EBS volumes can persist if instance is stopped, but not if instance is terminated.
** You can detach and re-attach EBS vols, and you can attach them to different instances (but
   only attach to one instance at a time, I think).
** By default, attached root EBS volumes are deleted when instance terminates, but there's a flag (DeleteOnTermination) to alter that.
*** EBS volumes that are attached *after* instance is created/running are *not* deleted; rather, they're just detached, and you can 
    attach them to other instances later.

Access requried:
* 

Create a Custom AMI from a running EC2 instance:
http://docs.aws.amazon.com/gettingstarted/latest/computebasics-linux/getting-started-create-custom-ami.html
1. Open EC2 Console
2. Make sure US East (N. Virginia) is selected (upper right)
3. click "Instances"
4. Assign name, etc
5. r-click your running instance, then "Create Image (EBS AMI)"
   - EC2 terminates the instance, makes images of all attached volumes, creates & registers new AMI,
     and restarts instance.
6. Note created AMI ID
7. Monitor status until "available"
8. click "Snapshots" to view.  Any instance created with this AMI uses the snapshot as its root device.
