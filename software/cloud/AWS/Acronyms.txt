AMI: Amazon Machine Image
ASW: Amazon Simple Workflow
AWS: Amazon Web Services
bucket: (S3) ???
CDN: Content Delivery Network ("CloudFront")
EBS: Elastic Block Storage
     Persistent independent of EC2 instances; can attach or detach
     Can also be started and stopped
EC2: Elastic Computing Cloud
EIC: Elastic IP address (free when used, so don't waste them)
ELB: Elastic Load Balancing
image: read-only copy of the initial state of an instance; contains all software, etc.
instance: a server
RDS: Relational Database Server
Route53: DNS web service
RRS: Reduced Redundancy Storage
SSS: Simple Storage Service
S3:  Simple Storage Service
SDB: SimpleDB (previously "Dynamo") (non-relational, aka "nosql", db)
SMS: Simple Message Service
SOA: Service Oriented Architecture
SQS: Simple Queue Service (message passing)
SWF: Simple Workflow Service
