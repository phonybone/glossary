Files: 

Workflow.java
* Defines WF interface
* one method/WF (theoretically more than one WF can be defined here) (I think)

WorkflowImpl.java
* Implements WF

WorkflowHost.java
* connects to SWF using configHelper/createSWFClient()
* creates wf workers using task lists, 
* calls worker.addWorkflowImpl(WorkflowImpl.class)
* calls worker.start(), which polls for requests

Activities.java
* Interface defining activities

ActivitiesImpl.java
* Must actually implement activities (steps)

ActivityHost.java
* connects to SWF using configHelper/createSWFClient()
* creates activity workers using task lists, 
* calls worker.addActvitiesImpl(new ActivitiesImpl())
* calls worker.start(), which polls for requests

WorkflowExecutionStarter.java
* calls workflow.<entrypoint>(<params>), as defined in Workflow.java

Questions:
* Where is domain defined?
** See ConfigHelper
** Domains are ref'd by *Host.java, WFExecutionStarter.java

* How do ActivityImpl's report success/failure?  
** with Exceptions?
