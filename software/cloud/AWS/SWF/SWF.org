Notes on implementing workflows
See http://docs.amazonwebservices.com/amazonswf/latest/developerguide/swf-dg-using-swf-api.html

* Creating a Basic Workflow
** Planning a workflow, registering its type, and registering its activity types
** Developing and launching activity workers that perform activity tasks
** Developing and launching deciders that use the workflow history to determine what to do next
** Developing and launching workflow starters, that is, applications that start workflow executions

* Registering a Domain
** See AmazonSimpleWorkflowClient::RegisterDomain()

* Registering a Workflow Type
** See AmazonSimpleWorkflowClient::RegisterWorkflowType()
** Register with type-id (name+version)

* Registering an Activity Type
** See AmazonSimpleWorkflowClient::RegisterActivityType()
** Register with type-id (name+version)
** Note: you don't have to tell AWS what program implements a particular activity type.  
   Rather, the implementing program polls for a specific type of activity that it can handle,
   and the Decider schedules tasks by telling AWS to do so.

* Developing an Activity Worker

* Polling for Activity Tasks
** See AmazonSimpleWorkflowClient::PollForActivityTask()
** task lists are an arg to above; 

* Performing the Activity Task
** See ActivityTask::getInput()

* Reporting Activity Task Heartbeats

* Completing or Failing an Activity Task

* Launching Activity Workers

* Developing Deciders
** On a state change (reported by ActivityWorkers?), SWF schedules a DecisionTask (ie, sends it to a decider)
** The Decider must:
*** Poll for DecisionTasks
*** Interpret the workflow execution history provided with the decision task
*** Apply the coordination logic based on the workflow execution history and makes 
    decisions on what to do next. Each decision is represented by a Decision structure
    (see Decision)
*** Complete the decision task and provides a list of decisions to Amazon SWF.
** Deciders can send back multiple Decisions, which presumably correspond to starting multiple jobs
** See http://docs.amazonwebservices.com/amazonswf/latest/apireference/API_Decision.html for a list of Decision Types
* Defining Coordination Logic
* Polling for Decision Tasks
** 

* Applying the Coordination Logic

* Responding with Decisions

* Closing a Workflow Execution

* Handling Errors

* Launching Deciders

* Starting Workflow Executions

* Notes on sample code
** HelloWorld "App"
*** Task is in ActivityHost.java
*** It creates an ActivityWorker object, then "populates" it with a HelloWorldActivities object, then calls worker.start()
*** Also uses a getRuntime().addShutdownHook()

*** Why no PollForActivityTask?
*** Why no RespondDecisionTaskCompleted?

From the FAQ:
* Domains: 
** Logical containers that hold application resources such as workflow types, activity types, or executions.  
** Also serves as a namespace.

* Deciders:
** Can be written in any language, like a worker
** Handles decision tasks
** Schedules tasks with SWF
** SWF maintains a history of the wf execution, makes this available to the decider
** History also includes input and output parameters to execution

* Task Lists:
** Task lists are SWF resources (Strings) that determine how tasks are assigned.
** Have user-defined names
** May contain tasks of different type-ids (but no mixing of activities and decisions)
** Task lists are defined by referencing them (kinda like MongoDB collections)
** Task list uses:
*** When initiating an activity, decdiers can add it into a specific task list or tell
    SWF to add it into the default task list for the activity type
*** When requesting tasks, deciders and workers specify which task list to ask from.
*** Settable<String>?
*** Lifetime trace (FileProcessing example):
**** Values created by ActivityHost, or from getHostName()
**** Stored in ActivityWorker(), which gets Impl. object added (ActivityHost creates as 
     many ActivityWorkers as it wants/needs; each one has a different task list that it polls)
**** That is, 1 AW <=> 1 AL
**** WF Impl uses options=new ActivitySchedulingOptions(tasklist, ...) object to assign jobs to specific
     tasklist (ie, "job queue").  It passes the options object to the ActivitiesClientImpl object that
     was generated from the ActivitiesImpl, using one of the six(?) overloaded methods generated.
* WF Executions:
** WF Executions are named by the app and are guaranteed to be unique (that is, you can't have 
  two executions with the same name at the same time).  This is useful for preventing
  conflicts, eg registering the same user twice.
** They also have unique run_ids, so that when an execution has terminated and then is 
  restarted, it can still be distiguished.  This helps with "retries".
 

* Walk-through demo:
** Used domain 'Sandbox'
** Inputs: which transform and which S3 bucket
** Workflow id: ImageProcessingWorkflow 1.0
** Activity ids: 
*** S3Download 1.0
*** SepiaTransform 1.0
*** GrayscaleTransform 1.0
*** S3Upload 1.0
