Artifact Registry

[[https://cloud.google.com/artifact-registry/docs][Docs Root]]
[[https://cloud.google.com/artifact-registry/docs/docker/store-docker-container-images][QuickStart]]


* push and pull images
[[https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling][Google Docs]]

** Authenticate to repository:
*** Using a credential helper
configured hosts are stored in $HOME/.docker/config.json
You only have to do this step once (maybe once/run during CI/CD?):

gcloud auth configure-docker us-west1-docker.pkg.dev
OR
docker-credential-gcr configure-docker us-west1-docker.pkg.dev

*** Using an access token
Google suggests using an access token together with SA impersonation:

gcloud auth print-access-token \
  --impersonate-service-account  ACCOUNT | docker login \
  -u oauth2accesstoken \
  --password-stdin https://LOCATION-docker.pkg.dev

*** Using a service account key
Not reccomended, but you can do it:
cat key.json | docker login -u _json_key_base64 --password-stdin https://us-west1-docker.pkg.dev

** Build docker image:
** Tag docker image:
docker tag ${IMAGE_NAME} ${IMAGE_TAG}

IMAGE_TAG is:
"${LOCATION}-docker.pkg.dev/${PROJECT_ID}/${REPOSITORY}/${IMAGE}[:${TAG}]"

where
LOCATION:  "us-west1-a"
PROJECT_ID: "freetier-219419"
REPOSITORY: <you choose> "gcf-artifacts"
IMAGE: geo-api
TAG: optional, defaults to "latest"

for a total of:
us-west1-docker.pkg.dev/freetier-219419/gcf-artifacts/geo-api:latest

Note 1: in the tag, the image_name doesn't have to be the same name
as from `docker image ls`, but does make sense for them to be the same.

** Get info about the repository:
gcloud artifacts repositories describe REPOSITORY \
    --project=PROJECT-ID \
    --location=LOCATION

** Push the tagged image to Artifact Registry
docker push LOCATION-docker.pkg.dev/PROJECT-ID/REPOSITORY/IMAGE:TAG

EG:
docker push us-west1-docker.pkg.dev/freetier-219419/gcf-artifacts/geo-api:latest


Note: GCP will create the artifact repo if it doesn't exist.

** Pull images:
Make sure you are [[https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling#auth][authenticated]] to the repo

docker pull LOCATION-docker.pkg.dev/PROJECT-ID/REPOSITORY/IMAGE:TAG
OR
docker pull LOCATION-docker.pkg.dev/PROJECT-ID/REPOSITORY/IMAGE@IMAGE-DIGEST

where IMAGE-DIGEST is the sha256 hash value of the contents.

*** Grant public access to a repo:
[[https://cloud.google.com/artifact-registry/docs/access-control#public][Google Docs]]

Basically:
gcloud artifacts repositories add-iam-policy-binding REPOSITORY \
   --location=LOCATION --member=allUsers --role=ROLE

EG:
gcloud artifacts repositories add-iam-policy-binding gitlab-runners \
  --location=us-west1 --member=allUsers --role=roles/artifactregistry.reader

** Debugging images:
There exists a tool call [[https://cloud.google.com/artifact-registry/docs/docker/pushing-and-pulling#pull-crictl][crictl]] that is useful for debuging images in
a K8 environment.

Also, here is a command for obtaining an access token for
authentication within the repo (presumably artifactory):

curl -s "http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/token" -H "Metadata-Flavor: Google"


* Gitlab integration
First have to configure Google IAM:

Project -> Settings -> Integrations -> Google Artifact Registry

[[https://stackoverflow.com/questions/43571787/docker-not-found-with-dockerdind-google-cloud-sdk][docker not found with docker:dind + google/cloud-sdk]] (StackOverflow)
* Videos
[[https://www.youtube.com/watch?v=cw34KMPSt4k][Video]] that mentions artifactory in a way that shows how to deploy a container for free.
It uses cloud run.

* Footer
