* Cloud Run
[[https://cloud.google.com/run][Root Document]]

Build and deploy scalable containerized apps written in any language
(including Go, Python, Java, Node.js, .NET, and Ruby) on a fully
managed platform.

Can be websites, APIs, one-off jobs; basically, any container.

Can be scheduled with Cloud Scheduler.


