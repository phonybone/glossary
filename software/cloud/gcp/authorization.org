GCP Authentication
* Overview:
https://cloud.google.com/docs/authentication/

Authorization Overview (IAM): 
https://cloud.google.com/iam/docs

Auditing Overview (Logs):
https://cloud.google.com/logging/docs/audit/

** Principals
A principle is an entity that can be granted access to a resource.  They
come in two flavors: 
*** user accounts
- manged by Google Accounts
- reprents a human

*** service accounts
- managed by IAM
- represents a non-human (eg, an App or Service or whatever)

** Applications
GCP APIs ONLY accept requests from registered applications.  They require
application credentials, which can be:
*** API Keys
https://cloud.google.com/docs/authentication/api-keys

An API key is a simple encrypted string that identifies an application
without any principal (aka user account or service account). They are
useful for accessing public data anonymously, and are used to
associate API requests with your project for quota and billing.

*** OAuth2 client credentials
https://cloud.google.com/docs/authentication/end-user
*** Service Account Keys
https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account
also https://cloud.google.com/iam/docs/understanding-service-accounts


** Authentication Strategies
There are all sorts of different ways to go about authentication, depending on what
you're doing.  If it's all "within" GCP, often it is done automagically and behind
the scenes.

GCP APIs use OAuth2 for both user and service accounts.  OAuth2 determines both
the principal and the application.

You can also use API keys for anonymous access to public data, which only ids the app.  If
a principal also needs to be authenticated, then you have to use other means as well.

There are multiple authentication flows that are supported by GCP APIs.  Google reccomends
using Google Cloud Client Libraries https://cloud.google.com/apis/docs/cloud-client-libraries.


** Server-server auth:
https://cloud.google.com/docs/authentication/production
In many GCP "native" environments (ComputeEngine, KubernetesEngine, AppEngine, Cloud Functions),
GCP can find the default service account provided by those services.  That means that 
when you're using a google client lib, you can skip providing the auth creds when
constructing various clients (eg a cloud storage client).

However, it's sometimes better to use an explicit service account so you know what's going on.

Before it does, however, it also checks for the env var GOOGLE_APPLICATION_CREDENTIALS.  This
should be set to the path that contains the .json that holds all the credential info.

If neither of these two approaches are acceptable, there is generally a method in the various
client libs that construct your client object using specied credentials.  EG, 

def explicit():
    from google.cloud import storage

    # Explicitly use service account credentials by specifying the private key
    # file.
    storage_client = storage.Client.from_service_account_json(
        'service_account.json')

    # Make an authenticated API request
    buckets = list(storage_client.list_buckets())
    print(buckets)

* gcloud cmds
gcloud auth login
gcloud auth application-default login

[[https://stackoverflow.com/questions/53306131/difference-between-gcloud-auth-application-default-login-and-gcloud-auth-logi][Difference between 'auth login' and 'auth application-default login']]:
The difference is in the use cases:

'gcloud auth login' puts your creds in ~/.config/gcloud, and now you
can run gcloud commands, but code/SDKs won't.  see [[https://cloud.google.com/sdk/gcloud/reference/auth/login][here]].

'gcloud auth application-default login' obtains your creds and stores
them in the "well-known location for Application Default
Credentials".  Now code/SDKs work.  This is a good stand-in when you
want to locally test code which would normally run on a server and use
a server-side credentials file.  More [[https://cloud.google.com/sdk/gcloud/reference/auth/application-default/login][here]].

And here: [[https://medium.com/google-cloud/local-remote-authentication-with-google-cloud-platform-afe3aa017b95][Local/Remote Authentication with GCP]].  This is a good
overview of `gcloud auth *`!

** gcloud auth list
Lists all accounts

** gcloud config set account 'ACCOUNT'
Sets active account, from list above

** gcloud auth application-default login

see https://cloud.google.com/sdk/gcloud/reference/auth/application-default/
and https://developers.google.com/identity/protocols/application-default-credentials?_ga=2.123488269.-72016709.1578590660
    for more on Application Default Credentials (ADC)

** gcloud auth print-identity-token
Prints the identity token; use with curl to (eg) invoke a cloud function:

curl https://REGION-PROJECT_ID.cloudfunctions.net/FUNCTION_NAME -H "Authorization: bearer $(gcloud auth print-identity-token)"

You can also get the application default access token:
gcloud auth application-default print-access-token

** grant or remove permissions to an SA

To change:
# Remove the Editor role
gcloud projects remove-iam-policy-binding PROJECT_ID \
  --member="PROJECT_ID@appspot.gserviceaccount.com"
  --role="roles/editor"

# Add the desired role
gcloud projects add-iam-policy-binding PROJECT_ID \
  --member="PROJECT_ID@appspot.gserviceaccount.com"
  --role="ROLE"


* Zonar/ROK8
** prepare-gcloud
Uses GOOGLE_APPLICATION_CREDENTIALS and `gcloud auth activate-service-account` to authorize

echo "${GCLOUD_KEY}" | base64 ${DECODE} > "$GOOGLE_APPLICATION_CREDENTIALS"
gcloud auth activate-service-account --configuration "rok8s-${GCP_PROJECT}" --key-file "$GOOGLE_APPLICATION_CREDENTIALS" --project "${GCP_PROJECT}"

How does the --key-file get populated?
In DRS/.gitlab-ci.yml, GOOGLE_APP_CREDS is hard-coded as /tmp/gcloud_key.json (and
then used later in .setup_working and .setup_production)
GCLOUD_KEY is defined in gitlab ci/cd settings (see line starting with 'echo "${GCLOUD_KEY}"...' above.

# Summary
Start with service account (existing or create new one)
Create/existing key for SA
Encrypt key using base64
Populate gitlab CI/CD variable using name=GCLOUD_KEY, value=base64(key)
use prepare-gcloud in your .gitlab-ci.yml; this calls `gcloud auth activate-service-account`

Then .gitlab-ci.yml calls helm-deploy, but there's nothing explicit in there about authorization,
so presumably the `gcloud auth activate-service-account` is still in effect.

Note that prepare-gcloud uses some gcloud-wide --flags, eg --configuration and --project

See also https://cloud.google.com/sdk/gcloud/reference/auth/activate-service-account


* This shows up in the gcloud shell you get from the console:
gcloud container clusters get-credentials vpc-native-working-1-kube-zonar --zone us-west1-a --project zonar-working



* Application Default Credentials:
[[https://cloud.google.com/docs/authentication/provide-credentials-adc][Google Docs]]

# Create credential file:
gcloud auth application-default login

This command places a JSON file containing the credentials you provide
(usually from your own Google Account) in a well-known location on
your file system. The location depends on your operating system:

Linux, macOS: $HOME/.config/gcloud/application_default_credentials.json ([[file:/~/.config/gcloud/application_default_credentials.json][link]])
(I think you can override this location by setting $GOOGLE_APPLICATION_CREDENTIALS;
see the above link for more details).

* python client lib(s)
[[https://googleapis.dev/python/google-auth/2.19.1/][google.auth package docs]]

* Google OIDC Authentication Tutorial
OIDC: OpenID Connect


** Get a token from the metadata server
For resources that can have an SA attached, there is a metaserver for those services
and that server can (usually) issue an ID token.
