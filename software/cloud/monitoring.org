* Datadog
** Monitoring
see https://docs.datadoghq.com/monitors/
Note: you can create monitors on just about anything, not just metrics.
The following discussion is mostly about *metric* monitors.

*** Monitor types: 
see https://docs.datadoghq.com/monitors/monitor_types/

- Threshold alert: compare the value in the selected timeframe against a given 
  threshold.  
- Change alert: compares absolute percentage or percentage change in value over
  a timeframe.  Uses parameters in "alert conditions" section.  Useful for tracking
  spikes/drops, et al.
- Anomaly Detection: algorithmic detection of uncommon behaviour.  Useful for metrics
  with predictable, periodic behaviour.
- Outlier Detection: algorithmic alert that compares one group to another.  EG one 
  webserver in a pool is processing many more/fewer requests than another.  Or that
  one availability zone is producing more errors than another.
- Forecast Detection: predict the future!!!

Monitors can be created on metrics that you want.

*** 

*** Creating a monitor:
1. Choose detection method (ie, monitor type, as above)
2. Choose the metric and scope.  Scope includes "from", "excluding", and "avg by"
   (recall that "avg by" is like GROUP BY in SQL);
3. Select alert grouping:
   Simple alerts: aggregate over all reporting sources.
   Multi alerts: gives you a per-source alert, eg per-machine
4. Alert conditions:
   - threshold (during the last X minutes)
   - on average


** Tags
see https://docs.datadoghq.com/tagging/using_tags

With Dashboards:
Use *from* textbox to filter tags on a graph; that is, adding tags to the *from* 
box filters the metric based on the tag's key/value.

Conversely, using the *avg by* box splits all values apart into separate graph lines
(or whatever the visualization uses?)  This is sort of like SQL's GROUP BY clause.

** Events
https://docs.datadoghq.com/api/?lang=python#events

** Python
Client: https://github.com/DataDog/datadogpy

README Includes links to:
- Library Documentation: http://datadogpy.readthedocs.org/en/latest/
- HTTP API Documentation: http://docs.datadoghq.com/api/
- DatadogHQ: http://datadoghq.com
