* Pandas 
basic tutorial:
https://towardsdatascience.com/python-for-data-science-part-3-be9b08660af9

tips & tricks:
https://realpython.com/python-pandas-tricks/
- configuration
- create fake/test datasets
- accessor methods
- categorical data
- introspection
- boolean ops
