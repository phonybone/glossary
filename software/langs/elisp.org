* Basics
[[https://www.gnu.org/software/emacs/manual/html_node/emacs/Lisp-Eval.html][Evaluating Emacs Lisp Expression]]
[[https://www.gnu.org/software/emacs/manual/html_node/eintr/][Introduction to Programming in Emacs Lisp]]
[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Dotted-Pair-Notation.html][Dotted Pair Notation]]
[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Building-Lists.html][Building Cons Cells and Lists]]

* GTD
[[https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html][Orgmode for GTD]]
[[https://orgmode.org/worg/org-tutorials/orgtutorial_dto.html][David O'Toole Org tutorial]]

* Common Lisp
https://nyxt.atlas.engineer/learn-lisp
