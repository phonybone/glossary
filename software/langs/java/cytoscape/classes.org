* AbstractCyActivator (C): 
** a BundleActivator with convenience methods for registering OSGi services.
** Apps implement this class and the start(BundleContext) method.
  

* AbstractCyApp (C):
** Primary app interface that all apps extend.  Gives access to Cy3 services,
   but knowledge of Maven/OSGi not required.  No access to Swing; use AbstractCySwingApp
   for that.  Sample05a registers service in constuctor (after calling super()).

* AbstractTask (C):
** Implements functionality of an App(?); 'run' method called automatically
   by task manager

* AbstractTaskFactory (C):
** Needed by apps, apparently; 



* CyAppAdapter (I): 
** provides access to various core interfaces, ie, to the app as a whole
** passed to implementations of AbstractTask (constructor)

