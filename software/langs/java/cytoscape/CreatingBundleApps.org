* Start with existing app (eg sample02, sample05a)
** cp -r sample05a <newdir>

* Modify pom.xml for name, groupId, articfactId, version, project deps in 
** <project>
     <properties>
       <bundle.symbolicName>network_stats</bundle.symbolicName>               !!!
       <bundle.namespace>org.cytoscape.sample.internal</bundle.namespace>     !!!
       <cytoscape.api.version>3.0.0-alpha8-SNAPSHOT</cytoscape.api.version>   !
       <maven-jar-plugin.version>2.3.1</maven-jar-plugin.version>            
       <maven-surefire-plugin.version>2.7.1</maven-surefire-plugin.version>  
     </properties>
       
** <build>
     <plugins>
       <plugin>
         <groupId> !!!
	 <artifactId> !!!
	 <version> !!!
	 <configuration>
	   <archive>
	     <manifestEntries>
	       <Cytoscape-App>${bundle.namespace}.${bundle.symbolic_name}</Cytoscape-App> !!!
               <Cytoscape-App-Name>Network_Stats</Cytoscape-App-Name>                     !!!
               <Cytoscape-App-Version>0.01</Cytoscape-App-Version>                        !!!
               <Cytoscape-App-Works-With>3.0</Cytoscape-App-Works-With>
	     </manifestEntries>


* Modify src/main/resources/plugin.props
** pluginName
** pluginDescription
** pluginCategory (optional?)
** pluginAuthorsInstitutions
** releaseDate

* extend AbstractCyActivator for the entry point
* mvn clean install
* use cy3 shell to install, start the jar file (below ~/.m2)

* Where to put UI stuff??
** See sample28:
*** two services registered in sample28/CyActivator: sample28Action, myCytoPanel
*** Sample28 extends AbstractCyAction, implements constructor and actionPerformed(event)
