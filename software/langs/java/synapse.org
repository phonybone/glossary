Synapse (Sage BioNetworks)

Concepts:
* Project

* Users

* Data Sets
** Annotated, kv pairs
** belongs to exactly one project

* Data Layers
** Subset of a Data Set; all data in data layer shares a similar structure
** most common example: data collected via the same experimental methodology

* Entities
