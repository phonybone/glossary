Three kinds of Views:
* Simple webpage
* web service
* non-HTML files (eg spreadsheets, CSV, whatevs)

To create: script/myapp_create.pl view <mod_name> <parent class?>

Catalyst looks for a template w/same name as subroutine, applying 
appropriate prefix and .ext.
* EG: Controller::Test::add() -> root/test/add.tt
