Dispatching Flow Chart:
* Prepare
* Find action URL or default
* Find and run most-specific 'begin'
* (4) Find and run least-specific 'auto'; goto Finish on die
* If any 'auto' returns 0, goto most specific 'end'
* While more specific 'auto' exists, run that (ie, goto 4)
* When done with all 'auto's, run found action or default
* find and run most specific 'end'
* Finish

Action attributes:
* Private:
** Cannot be accessed by visiting a URL
** Can only be accessed via forward() or detach() within a controller.
** Legacy exceptions: default :Private {} and index :Private {}

* Local:
** Matches <Controller>/<sub name>
* Global:
** Matches <subname>
* Path:
** Exactly like Local, except that the tail end of the path and the subname
   don't have to match
** IE: :Local == :Path(<subname>)
** EG: sub Root::hello : Local matches /hello
       sub Root::hi :Path('hello') also matches /hello
** sub index :Path :Args(0) matches
* LocalRegex:
* Regex:
* Chained("$method_name"):
** "automatic, configurable, cascading forwards"
** $method_name is preceding subroutine in chain, expressed as a path 
*** can be from a different controller, indicated by an /absolute/path

* PathPart()
* Args()
* CaptureArgs()

Special Dispatch Types:
* begin
** only most specific run
* auto
** all run, in order of least specific to most
* end
** only most specific run

Misc:
* You can add methods to $c by defining them in the top-level MyApp.pm
