
https://www.typescriptlang.org

TS Handbook: https://www.typescriptlang.org/docs/handbook/basic-types.html
Declaration Files: https://www.typescriptlang.org/docs/handbook/declaration-files/introduction.html

Deepdive: https://basarat.gitbook.io/typescript/
Sandbox: https://www.typescriptlang.org/play

Gulp: https://www.typescriptlang.org/docs/handbook/gulp.html

* Types
[[https://www.typescriptlang.org/docs/handbook/2/everyday-types.html][Basic (Everyday) Types]]
