* iptables
Definitive guide: http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch14_:_Linux_Firewalls_Using_iptables#.WLMfkCHythE

iptables does firewall and network address translation (NAT).
(It is basically a mini-router?)
iptable is superceeded by firewalld on some systems (eg RedHat/Centos).

Packet Processing:
There are three tables (aka "queues"): "mangle", "filter", "nat"
- mangle queue: responsible for altering QOS bits; rarely used in home/small office environs
- filter queue: packet filtering.  This is the one we care most about
- nat queue: 


** Basic packet processing:
Each chain/queue/table is comprised of rules.  Each rule can filter a packet, or change it?
Actually, there are a bunch of actions, called "targets" or "jumps", that a rule can apply.  They are:

ACCEPT:      we're done here, pass packet to the next step (application, forwarding, whatever)
DROP:        we're done here, packet is silently discarded
LOG:         packet is logged, iptables continues processing
REJECT:      like DROP, but an error message is sent back to the originating host
DNAT:        re-writes destination address of packet
SNAT:        re-writes source address of packet
MASQUERADE:  not sure..

You can add and remove rules using the iptables command.  iptables takes these options:
-t <table>    (default "filter")
-j <target>   where to go/what to do when packet matches
-A            append rule to end of chain
-F            flush (delete) all rules in selected table
-p <prot>     match protocol
-s <src ip>   match source IP
-d <dst ip>   match dest IP
-i <intf>     match input interface
-o <intf>     match output interface

Example:

iptables -A INPUT -s 0/0 -i eth0 -d 192.168.1.1  -p TCP -j ACCEPT

adds an ACCEPT action to any TCP packet from any source entering on eth0, destined for 192.168.1.1


** list rules
# list rules in command form (ie, you can use the output of this command as input to iptables; sort of like mysqldump)
iptables -S [<chain>] [-v]

# list rules in table form (better readability)
iptables -L [<chain>] [-v]

-v gives counts (both packet and bytes).  You can 'zero' the counts using the -Z flag; use <chain> and <rule no>
to zero out counts for specific chains/rules


** mangle queue (zzz...)
   
** filter queue
This is the default queue for the iptables command.

There are three "chains" in the filter queue:
- INPUT: filters packets destined for here
- OUTPUT: filters packets originating here
- FORWARD: filters packets destined for somewhere else

  

** open/close ports
Open port 80:
(see http://unix.stackexchange.com/questions/109443/open-port-80-in-centos-6-5)

sudo iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
sudo /etc/init.d/iptables save


* firewall-cmd
# see https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7

** zones
zones divides filtering rules based on environment.  For example, if you have a laptop that moves between
a secure environment (safe, a trusted office wifi network) and an insecure one (eg public wifi), you can
change your set of filters.  For servers, we can pretty much stick with one rule (a restrictive one, 
since servers are public-facing: public (see below)).

From most secure to least secure, the default zones are:
- drop: drop all requests with no error message
- block: drop all requests with error messages (eg icmp-host-prohibited)
- public: allows selected incoming messages.  This is what we will use
- external: for use when using a firewall as a gateway (eg, NAT server)
- internal: used for the internal portion of the gateway (NAT client)
- dmz
- work 
- home
- trusted: trust all machines on network

# get default zone:
firewall-cmd --get-default-zone

# change zone (for given interface):
firewall-cmd --zone=<zone> --change-interface=<intf>

Note: this might change the firewall rules for ssh, dropping your connection and 
locking you out of your system.  BE CAREFUL!

# changing zone across reboots:
# for one interface:
edit /etc/sysconfig/network-scripts/ifcfg-<intf>

and add:
ZONE=____

(then systemctl restart)

# for all interfaces:
firewall-cmd --set-default-zone=<zone>



** firewalld is a service
use systemctl to start/stop/restart/status/enable, etc
also firewall-cmd --state

** listing rules:
firewall-cmd --list-all

# (built on top of iptables)
# see https://www.digitalocean.com/community/tutorials/how-to-migrate-from-firewalld-to-iptables-on-centos-7
# see http://stackoverflow.com/questions/24729024/centos-7-open-firewall-port
firewall-cmd --get-active-zones
sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --reload

** managing services (eg http or ftp)
# list all available services:
firewall-cmd --get-services

# get details about a particular service:
less /usr/lib/firewalld/services/<service>.xml
less /etc/firewalld/services/<service.xml>

(or not; that folder doesn't exist on cynvenio.devbox)

# make a service available to a zone:
firewall-cmd --add-service=<service> --zone=<zone> 

(omit --zone to use default zone)

** making changes permanent
Use the --permanent flag with the above commands


** adding ports instead of services
If you add a well-known service, you're (probably) just opening a port.  If you want
to open different ports, use --add-port=

firewall-cmd --zone=<zone> --add-port=5000/tcp
firewall-cmd --zone=<zone> --add-port=5000-6000/tcp
firewall-cmd --list-ports

# Confusion: on cynvenio, 
firewall-cmd --list-services --permanent
# gives
dhcpv6-client ftp https ssh

# but
firewall-cmd --list-ports
# gives
433/tcp 80/tcp
 
# where is port 20? 22?

** pairing ports and services (so you don't forget later)
You can define your own services.  (Currently there are no 
defined custom services on cynvenio)

# Here's an example:
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Example Service</short>
  <description>This is just an example service.  It probably shouldn't be used on a real system.</description>
  <port protocol="tcp" port="7777"/>
  <port protocol="udp" port="8888"/>
</service>

# afterwards:
firewall-cmd --reload
firewall-cmd --get-services

** Creating zones
It's possible



* nmap
# Use to scan for open, filtered, closed ports (from remote host)
nmap -p 80,443 <ip>

** filtered vs closed
closed means the port is reachable, but no service is using it.  filtered means the port is not reachable,
probably due to a firewall dropping packets destined for that port.

