* NFS mounts on everest[56].cynvenio.com
# 10.100.100.211 is orion
yum install -f -y nfs-utils nfs-utils-lib
mount 10.100.100.211:/home/orion/FastQ-Store1 /mnt/orion_FastQ_Store1/

# then 
cd /usr/local/everest/data/var/UserData/
mv Lab Lab.bak
ln -s /mnt/orion_FastQ_Store1 Lab
# don't bother with vsFTP on internal machines

