* Bad Gateway errors
This happens anytime there is any kind of error between uwsgi and nginx.  This can include
the application not being correct, permission errors for nginx log and run files, selinux
weirdness.

** Troubleshooting:
Look in the following logs:
uwsgi main log
uwsgi vassal log
application log
nginx/error.log

Also, run the application both as a standalone app and via uwsgi.  Get the uwsgi command
from the vassal config (usually /etc/uwsgi/sites/<app>.ini)

# see also https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
* Tutorials
Python, uwsgi, & docker
https://medium.com/bitcraft/docker-composing-a-python-3-flask-app-line-by-line-93b721105777

** Docker-nginx-flask-uwsgi
https://pythonspeed.com/articles/schema-migrations-server-startup/
