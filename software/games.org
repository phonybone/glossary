* Algorithms
** A* path-finding
https://www.redblobgames.com/pathfinding/a-star/introduction.html

** Links
Various path-finding algos
https://www.redblobgames.com/pathfinding/tower-defense/

[[https://deepmind.com/blog/article/EigenGame][Game theory as an engine for large-scale data analysis]]

** Bezier curves
[[https://youtu.be/aVwxzDHniEw][The Beauty of Bezier Curves]] (Youtube 24:26)

* Tools
A tile-map editor in javascript
http://slicker.me/javascript/tile_map_editor.htm

Steam Game Buider
https://store.steampowered.com/app/929860/Game_Builder
https://www.blog.google/technology/area-120/create-3d-games-friends-no-experience-required/

Godot game builder
https://godotengine.org/features

Creating 3D objects with Blender and Python
https://www.youtube.com/watch?v=tsmkqU25_As

[[https://github.com/ksplatdev/DuckEngine/][DuckEngine: a 2D game engine for the web]] (github)
[[https://ksplatdev.github.io/DuckEngine/][DuckEngine docs]] (really spotty, generated docs w/o much overview)

[[https://www.youtube.com/watch?v=gB1F9G0JXOo&list=WL&index=1][Learn Unity in 7:24:39]] (youtube)
** Physics Engines
[[https://rapier.rs/docs/][Rapier]] is written in rust and is cross-platform.  Provides:
- collisions
- joint restraints
- contact events/sensors
- javascript bindings

* Hex grids and spheres
Planet generator (using racket): https://github.com/vraid/earthgen

Wrap-around hex tile maps on sphere: https://www.redblobgames.com/x/1640-hexagon-tiling-of-sphere/

Geodisc grid system (.pdf): http://webpages.sou.edu/~sahrk/dgg/pubs/gdggs03.pdf

SO: Covering Earth with hexagonal map tiles: https://stackoverflow.com/questions/749264/covering-earth-with-hexagonal-map-tiles
Has pointers to some of the solutions above, and others.

Amit: http://www-cs-students.stanford.edu/~amitp/gameprog.html#hex  <=== this whole pages seems to have a lot of useful information about games programming in general
and https://www.redblobgames.com/x/1640-hexagon-tiling-of-sphere/  

Discrete Global Grids: https://www.discreteglobalgrids.org/software/
Looks useful, but in C++

Algorithms for generating hex-grid spheres (SO):
https://stackoverflow.com/questions/46777626/mathematically-producing-sphere-shaped-hexagonal-grid
Contains some python code, plus answer 2 contains pointers to more formal approaches.

* A-life coding
** CodingAdventure series (youtube)
[[https://www.youtube.com/watch?v=r_It_X7v-1E][Ecosystem Simulation]]
[[https://www.youtube.com/watch?v=bqtqltqcQhw][Boids]]
[[https://www.youtube.com/watch?v=X-iSQQgOd1A][Ant and Slime Simulations]]

[[https://www.youtube.com/watch?v=lHHpMn2UK6s&t=0s][equilinox]]
* Fluff
[[https://www.youtube.com/watch?v=sLqXFF8mlEU][Cool demo of procedural planets]] (not a useful learning tool)

* Various project examples
** Geographical Adventures
[[https://github.com/SebLague/Geographical-Adventures][Repo]] (github)
[[https://www.youtube.com/channel/UCmtyQOKKmrMVaKuRXz02jbQ][YouTube Channel]]

He also does the "Coding Adventures" youtube vids which are good

