* Blog post on approach
1. https://www.xaprb.com/blog/2006/08/16/how-to-build-role-based-access-control-in-sql/
2. https://www.xaprb.com/blog/2006/08/18/role-based-access-control-in-sql-part-2/

* Video on complicated access/attribute-based permissions in typescript:
[[https://www.youtube.com/watch?v=5GG-VUvruzE][YouTube]]
Goes over a lib called [[https://clerk.com/docs/organizations/overview][clerk]]
Includes info on resource-based permissions, sharing, and quite a lot more.
Also has a [[https://github.com/WebDevSimplified/permission-system][github]] repo.
